#!/usr/bin/python

from distutils.core import setup

setup_args = {
    "name": 'damicore_mongo',
    "version": '0.1.1',
    "author": 'Jeovane Sousa based on original work by Bruno Kim Medeiros Cesar',
    "author_email": 'jeovane@usp.br',
    "license": 'GPLv2',
    "requires": ['igraph', 'munkres', 'subprocess32'],
    "packages": ['damicore'],
    }

setup(**setup_args)
