#!/usr/bin/python
# coding=utf-8
# alterado de sequential_run.py

import os
import shutil
import sys
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from bson.objectid import ObjectId

import subprocess32
import time
import random
import glob

import logging

# class StreamToLogger(object):
#    """
#    Fake file-like stream object that redirects writes to a logger instance.
#    """
#    def __init__(self, logger, log_level=logging.INFO):
#       self.logger = logger
#       self.log_level = log_level
#       self.linebuf = ''
#
#    def write(self, buf):
#       for line in buf.rstrip().splitlines():
#          self.logger.log(self.log_level, line.rstrip())


def move_collection_data(src, dest, doc=None):
    for data in src.find(doc):
        dest.insert_one(data)
        if dest.find_one(data):
            result = src.delete_one(data)
            if result.deleted_count == 0:
                raise ValueError('Error deleting _id:%s from %s' % (data, src.full_name))
        else:
            raise ValueError('Error inserting _id:%s in %s' % (data, dest.full_name))
#logging
# logging.basicConfig(filename='/home/lsee/results.log', filemode='a', level=logging.DEBUG)
# stdout_logger = logging.getLogger('STDOUT')
# sys.stdout = LoggerWriter(stdout_logger, logging.debug)
# stderr_logger = logging.getLogger('STDERR')
# sys.stderr = LoggerWriter(stderr_logger, logging.warning)
# logging.basicConfig(
#    level=logging.DEBUG,
#    # format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
#    format='%(asctime)s:%(name)s:\t%(message)s',
#    filename="/home/lsee/results.log",
#    filemode='a'
# )
#
# stdout_logger = logging.getLogger('STDOUT')
# sl = StreamToLogger(stdout_logger, logging.CRITICAL)
# sys.stdout = sl
#
# stderr_logger = logging.getLogger('STDERR')
# sl = StreamToLogger(stderr_logger, logging.ERROR)
# sys.stderr = sl
# print 'teste'
# raise ValueError('teste')

# Iniciar comunicação com mongoDB
connection_str = 'mongodb://192.168.80.143:27017/Feeder01'
income_data_collection = 'incomeFaults'
historical_collection = 'FaultsAG'
results_collection = 'resultsAG'
temp_collection = 'FaultsAG_temp'
results_dir = '/home/lsee/results'

# create a connection
client = MongoClient(connection_str)

# check if the server is available
try:
    # The ismaster command is cheap and does not require auth.
    client.admin.command('ismaster')
except ConnectionFailure:
    sys.stderr.write("\nServer not available")

# select database
db = client.get_database()

# list collections on database
collections = db.collection_names(include_system_collections=False)

# check if historical_collection exists
if historical_collection not in collections:
    raise ValueError('Collection %s not in %s' % (historical_collection, collections))

# check if income_data collection exists
if income_data_collection not in collections:
    raise ValueError('Collection %s not in %s' % (income_data_collection, collections))

# check if result_data collection exists
if results_collection not in collections:
    raise ValueError('Collection %s not in %s' % (results_collection, collections))

income_data_collection = db[income_data_collection]
temp_collection = db[temp_collection]
historical_collection = db[historical_collection]
results_collection = db[results_collection]

# Preparando dados para minerar e ajustando condições iniciais
sys.stderr.write('moving initial files')
# Criar uma coleção temporaria e mover os dados pra lá
move_collection_data(historical_collection, temp_collection)
move_collection_data(income_data_collection, temp_collection)

docs_number = 4

initial_docs = docs_number + temp_collection.count()%docs_number
# initial_docs = 4
random_docs = temp_collection.aggregate([ {"$sample": { "size": initial_docs } } ])
[move_collection_data(temp_collection, historical_collection,doc) for doc in random_docs]
# move_collection_data(temp_collection, historical_collection, temp_collection.find_one())
# move_collection_data(temp_collection, historical_collection, temp_collection.find_one())

sys.stderr.write('\nremoving previous files')

# apagar os dados do banco de dados results
result = results_collection.delete_many({})
# apagar a pasta results
shutil.rmtree(os.path.join(results_dir, income_data_collection.name), True)

count = historical_collection.find().count()
sys.stderr.write('\nInitial Mining: %s\n' % count)


# executar o damicore para gerar o result inicial
t = time.time()

# output = subprocess32.check_output(['./scratchMongo.py',
subprocess32.call(['./scratchMongo.py',
                   connection_str,
                   income_data_collection.name,
                   historical_collection.name,
                   results_collection.name,
                   '--normalize-weights',#'--normalize-matrix',
                   '--results-dir', results_dir,
                   # '--auto-move-files',
                   '--o', '/dev/null'],
                                   # stderr=subprocess32.STDOUT,)

                    # stderr=log,
                  )

elapsed = time.time() - t # tempo de processamento do damicore
# print('\n' + output)
sys.stderr.write('\nTotal Elapsed time (seconds):\t%s' % elapsed)

if results_collection.count() != 1:
    raise ValueError('Error saving initial NCD matrix')

# sys.stderr.write("\nUsing DAMICORE based on previous NCD matrix and one new random file")
count = temp_collection.count()
data_ids = list(temp_collection.find({},{'_id':1}))
random.shuffle(data_ids)
# count2 = 0

for count2 in range(0,count,docs_number):
# for _id in data_ids:
    # os.system('clear')
    # count2+=1
    # sys.stderr.write('\n#############\nProcessing files %d-%d of %d, id: %s\n' % (count2+1,count2+docs_number,count,data_ids[count2:count2+docs_number]))
    sys.stderr.write('\n#############\nProcessing files 1-%d of %d\n' % (count2+docs_number,count))
    # mover dados da pasta de dados para a past income
    [move_collection_data(temp_collection, income_data_collection,_id) for _id in data_ids[count2:count2+docs_number]]
    # move_collection_data(temp_collection,income_data_collection,_id)
    # executar o damicore parcial até acabar os dados
    t = time.time()
    subprocess32.call(['./scratchMongo.py',
                       connection_str,
                       income_data_collection.name,
                       historical_collection.name,
                       results_collection.name,
                       # 'last',
                       # '--partial-ncd-mode',
                       '--normalize-weights',
                       # '--normalize-matrix',
                       '--results-dir', results_dir,
                       '--auto-move-files',
                       '--o', '/dev/null'
                       ])
    elapsed = time.time() - t
    sys.stderr.write('\nTotal Elapsed time (seconds):\t%s' % elapsed)

sys.stderr.write('\nMining done!')

sys.stderr.write('\nGenerating tree png')
for k in glob.glob(os.path.join(results_dir, income_data_collection.name,'*.nex')):
    sys.stderr.write('\n'+str(k))
    os.system('java -jar /home/lsee/Desktop/dependencies/FigTree_v1.4.3/lib/figtree.jar -graphic PNG %s %s.png > /dev/null' % (k,k))

sys.stderr.write('Process finished')
