#!/bin/bash

base_path=~/Desktop/damicorepy-master-python2/damicore
#income_path=$base_path/new_texts
#data_pool=$base_path/DataPool
#done_path=$base_path/texts
#results_path=$base_path/results

income_path=/home/lsee/Dropbox/Dados/income_path
data_pool=/home/lsee/Dropbox/Dados/AG
done_path=/home/lsee/Dropbox/Dados/AG_done
results_path=/home/lsee/Dropbox/Dados/DADOSResults

#tree_output=texts.newick
#output=texts.clusters
#graph_image=texts.svg
#ncd_output=texts.csv 

tree_output=/home/lsee/Dropbox/Dados/DADOSResults/tree.newick
output=/home/lsee/Dropbox/Dados/DADOSResults/clusters.clusters
graph_image=/home/lsee/Dropbox/Dados/DADOSResults/clusters.svg
ncd_output=/home/lsee/Dropbox/Dados/DADOSResults/ncd.csv 

LOG_FILE=$0.log

cd $base_path

clear
#log console
exec > >(tee -a ${LOG_FILE}) 2>&1

echo $(date --rfc-3339=seconds)
echo Moving initial files
mv $done_path/* $data_pool
mv $income_path/* $data_pool

# dependente dos arquivos utilizados
#mv $data_pool/*-1.txt $done_path
mv $data_pool/1 $done_path
mv $data_pool/10 $done_path
mv $data_pool/100 $done_path

echo Removing previous files
rm $tree_output $output $graph_image $ncd_output
rm -r $results_path/*

count=$(ls -l $done_path | grep -v ^d | grep -v ^t | wc -l)

echo Inital Mining: $count files 

# Ordinary Damicore
time python ./scratch.py --tree-output $tree_output --output $output \
  --graph-image $graph_image --matrix-format csv --ncd-output $ncd_output \
  --results-dir $results_path $done_path

if [[ ! -e $ncd_output ]]; then
	echo Error creating initial NCD matrix
	exit 1
fi

echo Using DAMICORE based on previous NCD matrix and one new random file from the data pool

count=$(ls -l $data_pool | grep -v ^d | grep -v ^t | wc -l)
count2=0

for i in $( ls $data_pool | shuf ); do
	let count2++
	echo Processing file $count2 of $count: $i
	mv $data_pool/$i $income_path
	# bug fixing: wait until previous file is moved: pause dropbox syncing
	while [[ $(ls -l $income_path | grep -v ^d | grep -v ^t | wc -l) -gt 1 ]]; do
		echo $(ls $income_path)
		sleep 1
	done
	# Damicore with Partial NCD 
	time python ./scratch.py --tree-output $tree_output --output $output \
	  --graph-image $graph_image --matrix-format csv --ncd-output $ncd_output \
	  --results-dir $results_path --partial-ncd-mode --auto-move-files \
	  $ncd_output $income_path $done_path
	echo Waitting move file $done_path/$i \!
	echo .
	echo $(ls -l $income_path | grep -v ^d | grep -v ^t | wc -l)
	while [[ ! -e $done_path/$i ]]; do
		echo Waitting move file $done_path/$i \!
	done	

done

for k in $( ls $results_path/income_path/*.nex ); do
	echo $k
	java -jar /home/lsee/Desktop/dependencies/FigTree_v1.4.3/lib/figtree.jar -graphic PNG $k $k.png > /dev/null
done


echo fim



 
