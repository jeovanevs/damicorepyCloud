#!/usr/bin/python
# coding=utf-8

import os
import sys
import re
import argparse
import csv
import tempfile

from pprint import pformat

import igraph

import _utils
import ncd2 as ncd
import ncd_base as ncdb
import partition as p
import clustering as c
import compressor as comp
import tree_simplification as nj
from tree import to_graph
from datasource import create_factory

from tree import newick_format
from _utils import normalize_list, dir_basename, open_outfile, get_version

import damicore
from NexusColorClustersWriter import NexusColorClustersWriter
import shutil

########################################3
# TODO(brunokim): use a cladogram layout, which igraph seems to be lacking
def graph_style(g, ids):
    style = {}
    seed_layout = g.layout('rt_circular')

    layout = g.layout('fr', seed=seed_layout.coords, weights='weight')
    style['layout'] = layout

    is_leafs = [(v['name'] in ids) for v in g.vs]
    style['vertex_size'] = [15 if is_leaf else 5 for is_leaf in is_leafs]
    style['vertex_label'] = [v['name'] if is_leaf else ''
                             for is_leaf, v in zip(is_leafs, g.vs)]

    return style


####################################
def ncd_individual_compressed_sizes(ncd_results_list):
    """Converts a list of NcdResults to a dict {id:value} of compressed values.

     @param ncd_results_list List of NcdResult as returned by distance_matrix
     @param is_self Whether the results are from a self-distance computation
     @return {ids:values} compression values of corresponding IDs
     """
    xs = [(result['x'], result['zx']) for result in ncd_results_list]
    ys = [(result['y'], result['zy']) for result in ncd_results_list]
    comp_sizes = sorted(set(xs + ys))

    return dict(comp_sizes)


####################################
def parse_args(args):
    if len(args.input) < 3 or not os.path.isfile(args.input[0]):
        raise ValueError(
            'Invalid input arguments: Provide at least the previous NCD matrix file, the path to new files, and the path to previous files as '
            'inputs, respectively ')
    elif len(args.input) > 4:
        raise ValueError('More than three inputs provided')

    # load the previous NCD results from input file
    previous_ncd_results = ncd.FilenameNcdResults(args.input[0])
    previous_results = list(previous_ncd_results.get_results())

    # read the individually compressed sizes from the previous NCD results
    individual_compressed_sizes = ncd_individual_compressed_sizes(previous_results)

    # create the new source factory form input path
    factory = create_factory(args.input[1])

    # create the previous source factory form input path
    previous_factory = create_factory(args.input[2])

    # get the previous source names
    previous_source_names = [source_file.name for source_file in previous_factory.get_sources()]

    # check if some source name in the new datasource exist in the previous NCD file and ask what to do
    replaced_files = []
    replace = 'N'
    auto_move_files = args.auto_move_files
    source_names = [source_file.name for source_file in factory.get_sources()]
    same_name_sources = [k for k in source_names if individual_compressed_sizes.has_key(k)]
    if len(same_name_sources) > 0:
        if verbosity >= 1:
            if len(factory.fnames) == len(same_name_sources):
                sys.stderr.write('##### All files in the present datasource are in the previous ncd matrix. ##### %s '
                                 '\r\n ' % same_name_sources)
            else:
                sys.stderr.write('\nThe following sources have the same name in the previous ncd matrix: \n%s \n' % same_name_sources)
            replace = raw_input('\r\nReplace previous NCD results? [Y/N] or Ctrl+c to cancel: ').capitalize()
            while replace not in ['Y', 'N']:
                sys.stderr.write('Invalid answer!')
                replace = raw_input('Replace previous results? [Y/N] or Ctrl+c to cancel: ').capitalize()

        else:
            replace = 'Y'

        if replace == 'Y':
            # TODO: verbosity considerations
            if verbosity >= 1:
                sys.stderr.write('Replacing previous NCD results')
            # If some file in the previous datasource have the same name in the new datasource remove the file from the previous data source
            replaced_files = [name for name in source_names if name in previous_source_names]
            [previous_source_names.remove(names) for names in replaced_files]

        else:
            # Todo
            # If all files are the same and no replacement to do raise an error.
            if len(factory.fnames) == len(same_name_sources):
                raise ValueError('Nothing to do!')
            if verbosity >= 1:
                sys.stderr.write('Keeping previous NCD results')
            replaced_files = []
            auto_move_files = False

            for name in same_name_sources:
                for source in factory.sources:
                    if source.name == name:
                        factory.fnames.remove(source.fname)
                        factory.sources.remove(source)

    # check if the NCD matrix files are in the data source with the previous files
    exclude_files_from_datasource = replaced_files[:]  # Shallow copy
    checked_files = []
    for name in previous_source_names:
        if name in individual_compressed_sizes.keys():
            checked_files.append(name)
        else:
            exclude_files_from_datasource.append(name)

    # Raise an error if some file is missing
    if len(checked_files) != len(individual_compressed_sizes) - len(replaced_files):
        raise ValueError('The following files were not founded: %s' %
                         [file for file in individual_compressed_sizes.keys() if
                          file not in checked_files + replaced_files])

    # If there are files that are not in the NCD matrix, or replaced file, remove it from datasource factory
    if len(exclude_files_from_datasource) > 0:
        for name in exclude_files_from_datasource:
            for source in previous_factory.sources:
                if source.name == name:
                    previous_factory.fnames.remove(source.fname)
                    previous_factory.sources.remove(source)

    factories = [factory, previous_factory]

    compressor = comp.get_compressor(args.compressor, level=args.level,
                                     model_order=args.model_order, memory=args.memory,
                                     restoration_method=args.restoration)

    # retornar o par comprimido de acordo com os arquivos restantes
    if replace == 'Y':
        sources1 = sources2 = [source.name for source in previous_factory.get_sources()]
        pair_compressed_sizes = [result for result in previous_results for src1, src2 in
                                 ncd.pairs(sources1, sources2, is_upper=True) if
                                 (result['x'] == src1 and result['y'] == src2) or (
                                     result['y'] == src1 and result['x'] == src2)]

        individual_compressed_sizes = {k: individual_compressed_sizes[k] for k in sources1}
    else:
        pair_compressed_sizes = previous_results

#    if args.auto_move_files and replace=='Y': auto_move_files = True
#    else: auto_move_files = False

    return {
        'factories': factories,
        'compressor': compressor,
        'matrix_format': args.matrix_format,
        'individual_compressed_sizes': individual_compressed_sizes,
        'pair_compressed_sizes': pair_compressed_sizes,
        'previous_results': previous_results,
        'auto_move_files' : auto_move_files
    }


####################################
def partial_distance_matrix(factories, compressor, is_parallel=True,
                            individual_compressed_sizes=None, pair_compressed_sizes=None,
                            verbosity=0, tmp_dir=tempfile.gettempdir(),
                            interleave_block_size=0):
    """Calculates matrix of distances between all sources provided by a factory.
    """
    if not factories or len(factories) != 2:
        raise ValueError, 'Invalid factories argument'

    # testar se factories estão de acordo
    if len(factories[0].fnames) < 1:
        sys.stderr.write('No new file to compress, using previous NCD data\n')
        return  previous_results # retornar os valores antigos
    else:
        factory1, factory2 = factories
        sources1, sources2 = factory1.get_sources(), factory2.get_sources()
        sources = sources1 + sources2

    if verbosity >= 1:
        sys.stderr.write('Compressing individual sources...\n')
    compressed_sizes = ncd.compress_sources(sources1, compressor, is_parallel)
    compressed_sizes.update(individual_compressed_sizes)

    if verbosity >= 1:
        sys.stderr.write('Compressing source pairs...\n')

    # Create (nxn)-n matrix with new files
    partial_pairs = ncd.ncd_pairs(sources1, sources1, compressor, compressed_sizes,
                                  is_upper=True, is_parallel=is_parallel, tmp_dir=tmp_dir,
                                  interleave_block_size=interleave_block_size)

    partial_pairs = list(partial_pairs.get_results())

    # Create (mxn) matrix with new files and previous files
    if len(sources2) > 0:
        partial_pairs2 = ncd.ncd_pairs(sources1, sources2, compressor, compressed_sizes,
                                       is_upper=False, is_parallel=is_parallel, tmp_dir=tmp_dir,
                                       interleave_block_size=interleave_block_size)

        partial_pairs2 = list(partial_pairs2.get_results())
    else:
        partial_pairs2 = []

    # sort_results = lambda rs: sorted(list(rs), key=lambda r: (r['x'], r['y']))

    return sorted(partial_pairs + partial_pairs2 + pair_compressed_sizes,
                  key=lambda r: (r['x'], r['y']))


####################################
# clustering.pipeline function considering previously computed NCD and new files
def pipeline2(factories=None, compressor=None, ncd_results=None,
              individual_compressed_sizes=None, pair_compressed_sizes=None,
              is_parallel=True,
              is_normalize_matrix=True, is_normalize_weights=True, num_clusters=None,
              community_detection_name='fast', verbosity=0,
              is_partial_ncd=False, auto_move_files=False):
    if verbosity >= 1:
        if is_partial_ncd:
            sys.stderr.write('Performing partial NCD distance matrix calculation with new files...\n')
        elif ncd_results:
            sys.stderr.write('Using already computed NCD distance matrix\n')
        else:
            sys.stderr.write('Performing NCD distance matrix calculation...\n')

    if not is_partial_ncd:
        return c.pipeline(factories, compressor, ncd_results,
                          is_parallel=is_parallel,
                          is_normalize_weights=is_normalize_weights,
                          is_normalize_matrix=is_normalize_matrix,
                          num_clusters=num_clusters,
                          community_detection_name=community_detection_name,
                          verbosity=verbosity)
    else:

        ncd_results_list = partial_distance_matrix(factories, compressor,  # generator to ncd_results
                                                   individual_compressed_sizes=individual_compressed_sizes,
                                                   pair_compressed_sizes=pair_compressed_sizes,
                                                   is_parallel=is_parallel, verbosity=verbosity)

        if auto_move_files:
            dest = os.path.split(factories[1].fnames[1])[0]# dest = a.input[2]
            for src in factories[0].fnames:
                shutil.move(src, os.path.join(dest,os.path.split(src)[1]))

        return _pipeline(ncd_results_list, is_normalize_matrix, is_normalize_weights,
                         num_clusters, community_detection_name, verbosity)


###############################################################################################
def _pipeline(ncd_results_list,
              is_normalize_matrix=True, is_normalize_weights=True, num_clusters=None,
              community_detection_name='fast', verbosity=0):
    # ncd_results_list = list(ncd_results_list.get_results())  # previously generated results

    if verbosity >= 1:
        sys.stderr.write('Simplifying graph...\n')
    if not is_normalize_matrix:
        m, (ids, _) = ncd.to_matrix(ncd_results_list)
    else:
        ds = normalize_list(list(result['ncd'] for result in ncd_results_list))
        normalized_results = [ncd.NcdResult(result, ncd=dist)
                              for result, dist in zip(ncd_results_list, ds)]
        m, (ids, _) = ncd.to_matrix(normalized_results)

    tree = nj.neighbor_joining(m, ids)

    if verbosity >= 1:
        sys.stderr.write('Clustering elements...\n')
    g = to_graph(tree)
    membership, clustering, dendrogram = c.tree_clustering(g, ids,
                                                           is_normalize_weights=is_normalize_weights,
                                                           num_clusters=num_clusters,
                                                           community_detection_name=community_detection_name)

    return {
        'ncd_results': ncd_results_list,
        'phylo_tree': tree,
        'leaf_ids': ids,
        'tree_graph': g,
        'dendrogram': dendrogram,
        'membership': membership,
        'clustering': clustering,
    }


###############################################################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(parents=[_utils.cli_parser(), ncd.cli_parser(), c.cli_parser()],
                                     fromfile_prefix_chars='+',
                                     description="Compute the clustering of a given dataset using the DAMICORE" +
                                                 " methodology.")


    parser.add_argument('--ncd-output', help='File to output NCD result',
                        metavar="FILE")
    parser.add_argument('--tree-output', help='File to output tree result',
                        metavar="FILE")
    parser.add_argument('--graph-image', help='File to output graph image',
                        metavar="FILE")


    parser.add_argument('--results-dir', metavar="DIR",
                        help='Directory to output all obtained results')

    parser.add_argument('--partial-ncd-mode', action='store_true',
                        help='Compute compressions of the new files only')


    parser.add_argument('--auto-move-files', action='store_true',
                        help='Move new source files to the previous NCD source folder after'
                             'NCD calculation')



    partition_group = parser.add_argument_group('Clustering comparison options',
                                                'Options for comparison between partitions')
    partition_group.add_argument('--compare-to',
                                 help='Reference membership file to compare the resulting clustering',
                                 metavar="FILE")
    partition_group.add_argument('--partition-index', choices=p.list_indices(),
                                 default='wallace', help='Partition comparison index to use')
    partition_group.add_argument('--partition-output', metavar="FILE",
                                 help="File to output partition comparison (default:stdout)")

    a = parser.parse_args()
    general_args = _utils.parse_args(a)
    clustering_args = c.parse_args(a)
    output, is_parallel, verbosity = (general_args['output'],
                                      general_args['is_parallel'], general_args['verbosity'])
    is_normalize_weights, is_normalize_matrix, num_clusters = (
        clustering_args['is_normalize_weights'],
        clustering_args['is_normalize_matrix'], clustering_args['num_clusters'])
    community_detection_name, ncd_results = (
        clustering_args['community_detection_name'],
        clustering_args['ncd_results'])

    if not a.partial_ncd_mode:
        # TODO: implementar função
        print('Using Standard DAMICORE')
        outputs = damicore.main(a)
        
        # Outputs clustered philotree results in a Nexus format
        if outputs['tree_file'] and outputs['clusters_file']:
            nexus = NexusColorClustersWriter()

            with open(outputs['tree_file'], 'rt') as f:
                nexus.read_tree_string(f.read())

            with open(outputs['clusters_file'], 'rt') as f:
                nexus.read_clusters_string(f.read())

            nexus.write_to_file(os.path.splitext(outputs['tree_file'])[0] + '.nex')
        elif outputs['results_dir_base']:
            base = outputs['results_dir_base']
            NexusColorClustersWriter(base + 'tree.newick',base + 'membership.csv',base + 'clustertree.nex')

    else:
        if a.matrix_mode:
            raise ValueError('matrix-mode does not work in partial-ncd matrix calculation')

        # parse input args
        partial_ncd_args = parse_args(a)

        factories, compressor, matrix_format = (partial_ncd_args['factories'],
                                                partial_ncd_args['compressor'],
                                                partial_ncd_args['matrix_format'])

        individual_compressed_sizes = partial_ncd_args['individual_compressed_sizes']
        pair_compressed_sizes = partial_ncd_args['pair_compressed_sizes']
        previous_results = partial_ncd_args['previous_results']
        auto_move_files = partial_ncd_args['auto_move_files']

        result = pipeline2(factories, compressor, ncd_results,
                           individual_compressed_sizes, pair_compressed_sizes,
                           is_parallel=is_parallel,
                           is_normalize_weights=is_normalize_weights,
                           is_normalize_matrix=is_normalize_matrix,
                           num_clusters=num_clusters,
                           community_detection_name=community_detection_name,
                           verbosity=verbosity, # corrigir: is_partial_ncd não é necessário
                           is_partial_ncd=True, auto_move_files=auto_move_files)

        ncd_results = result['ncd_results']
        phylo_tree = result['phylo_tree']
        tree_graph = result['tree_graph']
        leaf_ids = result['leaf_ids']
        membership = result['membership']
        clustering = result['clustering']
        dendrogram = result['dendrogram']

        # Outputs NCD step
        if a.ncd_output:
            with open(a.ncd_output, 'wt') as f:
                if matrix_format == 'phylip':
                    ncd.write_phylip(f, ncd_results)
                else:
                    ncdb.csv_write(f, ncd_results, output_header=True)

        # Outputs tree in Newick format
        if a.tree_output:
            with open(a.tree_output, 'wt') as f:
                f.write(newick_format(phylo_tree))

        # Outputs graph image
        if a.graph_image:
            tree_style = graph_style(tree_graph, leaf_ids)
            igraph.plot(clustering, target=a.graph_image, **tree_style)

        # Outputs clustering result
        with open_outfile(output) as f:
            f.write(p.membership_csv_format(membership))

        # Outputs clustered philotree results in a Nexus format
        if a.tree_output and output:
            nexus = NexusColorClustersWriter()

            with open(a.tree_output, 'rt') as f:
                nexus.read_tree_string(f.read())

            with open(output, 'rt') as f:
                nexus.read_clusters_string(f.read())

            nexus.write_to_file(os.path.splitext(a.tree_output)[0] + '.nex')



        # Outputs index, if comparison reference was provided
        if a.compare_to:
            with open_outfile(a.partition_output) as f:
                reference_cluster = p.membership_parse(a.compare_to, as_clusters=True)
                obtained_cluster = p.membership_to_clusters(membership)
                index = p.compare_clusters(reference_cluster, obtained_cluster,
                                           index_name=a.partition_index)
                f.write('%s\n' % pformat(index))

        # Output everything to directory
        if a.results_dir:
            # Create dir if it does not exist
            if not os.path.exists(a.results_dir):
                os.mkdir(a.results_dir)

            # Creates subdirectory containing results for this run
            subdirname = dir_basename(a.input[1])
            subpath = os.path.join(a.results_dir, subdirname)
            if not os.path.exists(subpath):
                os.mkdir(subpath)

            # Finds maximum index in subdirectory
            fnames = os.listdir(subpath)
            matches = [re.match('\d+', fname) for fname in fnames]
            indices = [int(match.group(0)) for match in matches if match]
            max_index = max([0] + indices)  # if indices is empty, return 0
            index = max_index + 1

            # Writes all results
            base = os.path.join(subpath, '%03d-' % index)
            with open(base + 'version', 'wt') as f:
                f.write(get_version())
            with open(base + 'args.txt', 'wt') as f:
                f.write('\n'.join(sys.argv[1:]))
            with open(base + 'ncd.csv', 'wt') as f:
                ncd.csv_write(f, ncd_results)
            with open(base + 'ncd.phylip', 'wt') as f:
                ncd.write_phylip(f, ncd_results)
            with open(base + 'tree.newick', 'wt') as f:
                f.write(newick_format(phylo_tree))
            with open(base + 'membership.csv', 'wt') as f:
                f.write(p.membership_csv_format(membership))
            if a.compare_to:
                with open(base + 'partition.csv', 'wt') as f:
                    all_indices = p.compare_clusters(reference_cluster, obtained_cluster,
                                                     index_name='all')
                    writer = csv.DictWriter(f, fieldnames=['index name', 'value'])
                    writer.writeheader()
                    writer.writerows({'index name': k, 'value': v}
                                     for k, v in all_indices.items())

            tree_style = graph_style(tree_graph, leaf_ids)
            igraph.plot(clustering, target=base + 'tree.svg', **tree_style)

            nexus = NexusColorClustersWriter()
            with open(base + 'tree.newick', 'rt') as f:
                nexus.read_tree_string(f.read())
            with open(base + 'membership.csv', 'rt') as f:
                nexus.read_clusters_string(f.read())
            nexus.write_to_file(base + 'clustertree.nex')
