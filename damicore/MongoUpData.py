#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 18:34:43 2017

@author: lsee

Upload fault data on folder to database
"""
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

import pprint  # pretty print

import datetime as dt
import time
import uuid
import os


def get_filenames(directory):
    fnames = sorted(os.listdir(directory))
    return [os.path.join(directory, fname)
            for fname in fnames
            if os.path.isfile(os.path.join(directory, fname))]


# função para selecionar uma coluna em uma matriz
# def nCol(mat,col)
mCol = lambda mat, col: [row[col] for row in mat]


def load_data(input_folder, fault_type, host='10.235.2.227:27017'):
    inputFolder = input_folder
    FaultTypes = ['AG', 'BG', 'CG', 'ABG', 'ACG', 'BCG', 'ABCG', 'AB', 'AC', 'BC', 'ABC']
    FaultType = fault_type
    if FaultType not in FaultTypes:
        print('WARNING: Fault type %s not in %s. Nothing done!' % (FaultType, FaultTypes))
        return

    if not os.path.isdir(inputFolder):
        raise IOError("Unable To open Folder %s" % inputFolder)

    fnames = get_filenames(inputFolder)

    header = ["V1", "V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11",
              "AV1", "AV2", "AV3", "AV4", "AV5", "AV6", "AV7", "AV8", "AV9", "AV10", "AV11",
              "I1", "AI1"]

    uuidNamespace = uuid.UUID('{00000000-0000-0000-0000-000000000000}')
    FaultDoc = []

    # Load data files into memory and create Fault Documents
    for count, fname in enumerate(fnames):
        matIN = []
        # import numpy as np
        # arrayIN = np.loadtxt(fileIN)

        # with open(fileIN) as f:
        #     for line in f:
        #         numbers_str = line.split()
        #         numbers_float = [float(x) for x in numbers_str]
        #         matIN.append(numbers_float)
        #
        # with open(fileIN) as f:
        #     for line in f:
        #         numbers_float = map(float, line.split())
        #         matIN.append(numbers_float)

        with open(fname) as f:
            matIN = [map(float, line.split()) for line in f.readlines()]

        if len(matIN[0]) != len(header):
            raise ValueError('Input values length different from header')

        basedate = dt.datetime.utcfromtimestamp(time.time() + 3600 * count)

        FaultDoc.append({
            'EventTime': basedate,
            'FaultLocation': os.path.split(fname)[1],
            'FaultData': [{
                              'MeasurementPoint': str(i),
                              'DCU_Id': uuid.UUID("100000000000000000000000000000%02d" % i),
                              'SM_Ids': [uuid.UUID("000000000000000000000000000000%02d" % i)],
                              # 'DCU_Id': uuid.uuid3(uuidNamespace, "dcu"+str(i)),
                              # 'SM_Ids': [uuid.uuid3(uuidNamespace, str(i))]
                              'V3PHIrms': mCol(matIN, header.index("V%d" % i)) if ("V%d" % i) in header else [],
                              'ThetaV3PHI': mCol(matIN, header.index("AV%d" % i)) if ("AV%d" % i) in header else [],
                              'I3PHIrms': mCol(matIN, header.index("I%d" % i)) if ("I%d" % i) in header else [],
                              'ThetaI3PHI': mCol(matIN, header.index("AI%d" % i)) if ("AI%d" % i) in header else []
                          } for i in sorted(set([int(h.translate(None, 'AVI')) for h in header]))]
        })

    # create a connection
    client = MongoClient(host=host)

    try:
        # check if the server is available
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        print("Server not available")

    # # list databases
    # dataBases = client.database_names()

    # create or select database
    # db = eval("client.fault"+FaultType)
    db = client['Feeder01']

    # list created collections on database
    collections = db.collection_names(include_system_collections=False)

    # create or select collection
    if 'Faults' + FaultType not in collections:
        coll = db.create_collection('Faults' + FaultType)
        db.command("collMod", coll.name,
                   validator={
                       "$and": [
                           {'EventTime': {"$type": "date"}},
                           {'FaultLocation': {"$type": "string"}},
                           {'FaultData': {"$elemMatch": {
                               'MeasurementPoint': {"$type": "string"},
                               'DCU_Id': {"$type": "binData"},
                               'SM_Ids': {"$type": "binData"},
                               'V3PHIrms': {"$elemMatch": {'$exists': True}},
                               'ThetaV3PHI': {"$elemMatch": {'$exists': True}}, "$and": [
                                   {"$or": [{'I3PHIrms': {'$elemMatch': {'$exists': True}}}, {'I3PHIrms': []}]},
                                   {"$or": [{'ThetaI3PHI': {'$elemMatch': {'$exists': True}}},
                                            {'ThetaI3PHI': []}]}]
                           }}}]
                   },
                   validationLevel="strict",
                   validationAction="error")
        coll.create_index([('EventTime', pymongo.ASCENDING)], unique=True)
        # return index information
        print(sorted(list(coll.index_information())))
    else:
        coll = db['Faults' + FaultType]

    # insert multiple documents
    result = coll.insert_many(FaultDoc)
    # confirm the insertion
    pprint.pprint(result.inserted_ids)

    print('%s docs inserted, %s docs in collection' % (len(result.inserted_ids), coll.count()))

    # list created collections on database
    collections = db.collection_names(include_system_collections=False)
    pprint.pprint(collections)

    # # return the first document pretty printed
    # pprint.pprint(coll.find_one())
    #
    # # print all documents in a collection
    # for doc in coll.find():
    #     pprint.pprint(doc)


    # close connection
    client.close()


if __name__ != '__main__':
    raise ImportError('This script will not word as module')

################ Main ########################
if __name__ == '__main__':
    inputFolder = r'F:\Jeovanevs\Desktop\Dropbox\LinuxSync\Dropbox\Dados'
    faultFolders = [os.path.join(inputFolder, folder) for folder in os.listdir(inputFolder) if os.path.isdir(os.path.join(
        inputFolder, folder))]
    for faultTypeFolder in faultFolders:
        load_data(faultTypeFolder, os.path.split(faultTypeFolder)[1])
