from flask import Flask, render_template, request,stream_with_context, Response, send_file

import os, re, shutil, time
import subprocess as sub

from shelljob import proc
import time

app = Flask(__name__)


def load_folder(input_path, normalize_weights, normalize_matrix):
    # ./damicore.py --serial  --results-dir results testdata/texts
    if not input_path or not os.path.isdir(os.path.join('./dados/entradas/', input_path)):
        return "Folder name '%s' not found!" % input_path
    # time.sleep(5) #just simulating the waiting period

    cmd = ["./damicore_partial.py", normalize_weights, normalize_matrix, "--results-dir", "./dados/resultados",
           "./dados/entradas/" + input_path]

    # p = sub.Popen(filter(None, cmd), stdout=sub.PIPE, stderr=sub.STDOUT)

    # output = p.communicate()

    output = sub.check_output(filter(None, cmd))

    return output

def max_index(subpath):
    # Finds maximum index in subdirectory
    fnames = os.listdir(subpath)
    matches = [re.match('\d+', fname) for fname in fnames]
    indices = [int(match.group(0)) for match in matches if match]
    return max([0] + indices)  # if indices is empty, return 0


@app.route("/")
def index(dirs=None):
    dirs = os.listdir('./dados/entradas/')
    # dirs.remove('resultados')
    return render_template("index.html", folders=dirs)


@app.route('/', methods=['POST'])
def form(outcome=None):
    folder = request.form['folder']
    normalize_matrix = request.form['normalize-matrix'] if request.form.get('normalize-matrix') else ''
    normalize_weights = request.form['normalize-weights'] if request.form.get('normalize-weights') else ''
    outcome = load_folder(folder, normalize_weights, normalize_matrix)
    if os.path.isfile('./static/last_tree.svg'): os.remove('./static/last_tree.svg')
    figure = '%03d-tree.svg' % (max_index('./dados/resultados/'+ folder))
    shutil.copy(os.path.join('./dados/resultados/'+ folder,figure),'./static/last_tree.svg')

    return render_template("/done.html", display=outcome, figure=time.time())
    # return figure

@app.route( '/evandro' )
def stream():
    g = proc.Group()
    p = g.run( [ "bash", "-c","./macro_evandro.sh" ] )
	
    def read_process():       
        while g.is_pending():
            lines = g.readlines()
            for proc, line in lines:
                yield '<br/>\n' + line + '\n'

    return Response( stream_with_context(read_process()), mimetype= 'text/html' )

# import eventlet
# eventlet.monkey_patch()	

# @app.route( '/stream' )
# def stream():
    # g = proc.Group()
    # p = g.run( [ "bash", "-c", "for ((i=0;i<10;i=i+1)); do echo $i; sleep 1; done" ] )

    # def read_process():       
        # while g.is_pending():
            # lines = g.readlines()
            # for proc, line in lines:
                # yield "data:" + line + "\n\n"

    # return Response( (read_process()), mimetype= 'text/event-stream' )
	
# @app.route('/page')
# def get_page():
    # return send_file('templates/macro.html')

# @app.route('/yield')
# def stream2():
    # def inner():
        # proc = sub.Popen(
            # ['dmesg'],             #call something with a lot of output so we can see it
            # shell=True,
            # stdout=sub.PIPE
        # )

        # for line in iter(proc.stdout.readline,''):
            # time.sleep(1)                           # Don't need this just shows the text streaming
            # yield line.rstrip() + '<br/>\n'

    # return Response(inner(), mimetype='text/html')  # text/html is required for most browsers to show th$


if __name__ == "__main__":
    app.run(debug=True)