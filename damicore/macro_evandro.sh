#!/bin/bash

base_path=/home/team/damicorepy-parcial/damicore/dados

data_pool=$base_path/datapool
done_path=$base_path/done
results_path=$base_path/resultados/Evandro/results


LOG_FILE=$0.log

clear
#log console
# exec > >(tee -a ${LOG_FILE}) 2>&1

echo $(date --rfc-3339=seconds)

printf "Folders for mining: \n$dirname$(ls -d $data_pool/*/)\n\n"

for i in $( ls -d $data_pool/*/ ); do
	faultType=$(basename $i)	
	for j in  $( ls -d $i*/ ); do
		faultREG=$(basename $j)
		count=$(ls -l $j | grep -v ^d | grep -v ^t | wc -l) 
		echo .
		echo Processing folder  /$faultType/$faultREG\/: $count files to mine
		if (( $count > 0 )); then       
			# Run Damicore
			time python ./damicore_partial.py --normalize-weights \
				--results-dir $results_path/$faultType $j > /dev/null
		
			# copy overwrite and delete folder and files
			echo .
			# echo Moving folder $j to $done_path/$faultType/$faultREG
			rsync -a --remove-source-files ${j%?} $done_path/$faultType
			# rsync -a --remove-source-files ${j%?} $done_path
			find $j -type d -empty -delete  
		else echo Empty Folder! Nothing to do.
		fi
	done
done
echo "**************"
echo "** Finished!! **"
echo "**************"


 
