#!/bin/bash

base_path=~/damicore-cloud 
LOG_FILE=$0.log

cd $base_path
clear
#log console
exec > >(tee -a ${LOG_FILE}) 2>&1
date
#time python ./sequential_run.py
time python ./sensibility_test.py
