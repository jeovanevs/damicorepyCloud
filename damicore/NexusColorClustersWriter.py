#!/usr/bin/python
# coding=utf-8
"""
Tools for writing a nexus file
"""
import collections
import os
import operator
import re
import sys

try:  # pragma: no cover
    from StringIO import StringIO
except ImportError:
    from io import StringIO

TEMPLATE = """
#NEXUS
%(comments)s
BEGIN TAXA;
	DIMENSIONS NTAX=%(ntax)d;
	TAXLABELS
%(taxlabels)s
END;

BEGIN TREES;
    TREE tree_1 = [&R] %(newick_str)s
END;

begin figtree;
	set appearance.backgroundColorAttribute="Default";
	set appearance.backgroundColour=#ffffff;
	set appearance.branchColorAttribute="User selection";
	set appearance.branchColorGradient=false;
	set appearance.branchLineWidth=1.0;
	set appearance.branchMinLineWidth=0.0;
	set appearance.branchWidthAttribute="Fixed";
	set appearance.foregroundColour=#000000;
	set appearance.hilightingGradient=false;
	set appearance.selectionColour=#2d3680;
	set branchLabels.colorAttribute="User selection";
	set branchLabels.displayAttribute="Branch times";
	set branchLabels.fontName="Aharoni";
	set branchLabels.fontSize=8;
	set branchLabels.fontStyle=0;
	set branchLabels.isShown=false;
	set branchLabels.significantDigits=4;
	set layout.expansion=0;
	set layout.layoutType="POLAR";
	set layout.zoom=0;
	set legend.attribute=null;
	set legend.fontSize=10.0;
	set legend.isShown=false;
	set legend.significantDigits=4;
	set nodeBars.barWidth=4.0;
	set nodeBars.displayAttribute=null;
	set nodeBars.isShown=false;
	set nodeLabels.colorAttribute="User selection";
	set nodeLabels.displayAttribute="Node ages";
	set nodeLabels.fontName="Aharoni";
	set nodeLabels.fontSize=11;
	set nodeLabels.fontStyle=0;
	set nodeLabels.isShown=false;
	set nodeLabels.significantDigits=4;
	set nodeShapeExternal.colourAttribute="User selection";
	set nodeShapeExternal.isShown=false;
	set nodeShapeExternal.minSize=10.0;
	set nodeShapeExternal.scaleType=Width;
	set nodeShapeExternal.shapeType=Circle;
	set nodeShapeExternal.size=4.0;
	set nodeShapeExternal.sizeAttribute="Fixed";
	set nodeShapeInternal.colourAttribute="User selection";
	set nodeShapeInternal.isShown=false;
	set nodeShapeInternal.minSize=10.0;
	set nodeShapeInternal.scaleType=Width;
	set nodeShapeInternal.shapeType=Circle;
	set nodeShapeInternal.size=4.0;
	set nodeShapeInternal.sizeAttribute="Fixed";
	set polarLayout.alignTipLabels=false;
	set polarLayout.angularRange=0;
	set polarLayout.rootAngle=0;
	set polarLayout.rootLength=100;
	set polarLayout.showRoot=false;
	set radialLayout.spread=0.0;
	set rectilinearLayout.alignTipLabels=false;
	set rectilinearLayout.curvature=0;
	set rectilinearLayout.rootLength=100;
	set scale.offsetAge=0.0;
	set scale.rootAge=1.0;
	set scale.scaleFactor=1.0;
	set scale.scaleRoot=false;
	set scaleAxis.automaticScale=true;
	set scaleAxis.fontSize=8.0;
	set scaleAxis.isShown=false;
	set scaleAxis.lineWidth=1.0;
	set scaleAxis.majorTicks=1.0;
	set scaleAxis.minorTicks=0.5;
	set scaleAxis.origin=0.0;
	set scaleAxis.reverseAxis=false;
	set scaleAxis.showGrid=true;
	set scaleBar.automaticScale=true;
	set scaleBar.fontSize=10.0;
	set scaleBar.isShown=false;
	set scaleBar.lineWidth=1.0;
	set scaleBar.scaleRange=0.05;
	set tipLabels.colorAttribute="User selection";
	set tipLabels.displayAttribute="Names";
	set tipLabels.fontName="Aharoni";
	set tipLabels.fontSize=17;
	set tipLabels.fontStyle=0;
	set tipLabels.isShown=true;
	set tipLabels.significantDigits=4;
	set trees.order=true;
	set trees.orderType="increasing";
	set trees.rooting=false;
	set trees.rootingType="User Selection";
	set trees.transform=true;
	set trees.transformType="cladogram";
end;
"""


class NexusColorClustersWriter:
    def __init__(self, tree_filename=None, clusters_filename=None, outfilename=None):
        self.comments = []
        self._clusters = None
        self._taxa = None
        self.tree = ''
        self.clusters_dict = None
        if tree_filename:
            self.read_tree_file(tree_filename)
        if clusters_filename:
            self.read_cluster_file(clusters_filename)
        if outfilename:
            return self.write_to_file(outfilename)

    def read_tree_file(self, filename):
        """
        Loads a Newidk File

        :param filename: filename of a newick file
        :type filename: string

        :raises IOError: If file reading fails.

        :return: None
        """
        self.tree_filename = filename
        self.short_filename = os.path.split(filename)[1]

        if not os.path.isfile(filename):
            raise IOError("Unable To Read File %s" % filename)

        if filename.endswith('.gz'):
            handle = gzip.open(filename, 'rb')  # pragma: no cover
        else:
            handle = open(filename, 'r')
        self._read_tree(handle)
        handle.close()

    def read_tree_string(self, contents):
        """
        Loads a newick formatted tree from a string

        :param contents: string or string-like object containing a tree
        :type contents: string

        :return: None
        """
        self.tree_filename = "<String>"
        self._read_tree(StringIO(contents))

    def _read_tree(self, handle):
        """Reads from a iterable object"""
        is_tree = re.compile(r"""(^\([(A-Z0-9_\-\.,:')]+;$)""", re.IGNORECASE)
        tree_in_line = list()
        for line in handle.readlines():
            try:  # deal
                line = line.decode('utf-8')
            except:
                pass
            line = line.strip()
            if len(line) == 0:
                continue
            elif line.startswith('[') and line.endswith(']'):
                continue
            tree_in_line.append(line)

        tree = "".join(tree_in_line)
        match = is_tree.match(tree)
        if not match:
            raise AssertionError("Tree don't match the format Newick")

        self.tree = tree

    def read_cluster_file(self, filename):
        """
        Loads a Cluster File

        :param filename: filename of the clusters file
        :type filename: string

        :raises IOError: If file reading fails.

        :return: None
        """
        self.cluster_filename = filename
        self.short_filename = os.path.split(filename)[1]

        if not os.path.isfile(filename):
            raise IOError("Unable To Read File %s" % filename)

        if filename.endswith('.gz'):
            handle = gzip.open(filename, 'rb')  # pragma: no cover
        else:
            handle = open(filename, 'r')
        self._read_clusters(handle)
        handle.close()

    def read_clusters_string(self, contents):
        """
        Loads and Parses a DAMICORE cluster from a string

        :param contents: string or string-like object containing the clusters
        :type contents: string

        :return: None
        """
        self.clusters_filename = "<String>"
        self._read_clusters(StringIO(contents))

    def _read_clusters(self, handle):
        """Reads from a iterable object"""
        is_cluster = re.compile(r"""([A-Z0-9_\-\.]+)(,\s*)(\d+)""", re.IGNORECASE)
        clusters_lines = dict()
        taxa = list()
        clusters = set()
        for i, line in enumerate(handle.readlines(), 1):
            try:  # deal
                line = line.decode('utf-8')
            except:
                pass
            line = line.strip()
            if len(line) == 0:
                continue
            elif i == 1 and line.__contains__('filename,cluster'):
                continue
            match = is_cluster.match(line)
            if not match:
                raise AssertionError("Clusters don't match the format FileName,ClusterNumber")
                continue
            # if len(match.group(1)) > 10: print("WARNING!: The TAXA names bigger than 10 characters will be truncated. "
                                               # "please verify if your Data still correct")
            # if (match.group(1)[0:10]) in self.tree:
            #     self.tree = self.tree.replace((match.group(1)[0:10]), match.group(3) + '--' + (match.group(1)[0:10]))
            #
            #     taxa.append(match.group(1)[0:10])
            #     clusters.add(match.group(3))
            #     clusters_lines[match.group(3) + '--' + (match.group(1)[0:10])] = match.group(3)
            # else:
            #     print("\n Unequal TAXA detected in files for the following taxa %s" % match.group(1)[0:10])
            #     raise SystemExit
            if (match.group(1)) in self.tree:
                # TODO: COLOCAR OS NUMEROS DOS CLUSTERS NA FRENTE DOS NOMES DAS TAXAS PARA FACILITAR NA VISUALIZAÇÃO
                self.tree = self.tree.replace(("'"+match.group(1)+"'"), "'"+ match.group(1) + '--' +match.group(3)+"'")
                taxa.append(match.group(1))
                clusters.add(match.group(3))
                clusters_lines[(match.group(1)) + '--' + match.group(3)] = match.group(3)
            else:
                print("\n Unequal TAXA detected in files for the following taxa %s" % match.group(1)[0:10])
                raise SystemExit

        self.clusters_dict = clusters_lines
        self._clusters = clusters

    def clean(self, s):
        """Removes unsafe characters"""
        _EMPTY = ''
        replacements = {
            ' ': _EMPTY, '\\': _EMPTY, ':': _EMPTY,
            '/': _EMPTY, '?': _EMPTY,  # '-': _EMPTY,
            '(': '_', ')': _EMPTY,
        }
        for f, t in replacements.items():
            s = s.replace(f, t)
        return s

    def _make_comments(self):
        """Generates a comments block"""
        return "\n".join(["[%s]" % c.ljust(70) for c in self.comments])

    def add_comment(self, comment):
        """Adds a `comment` into the nexus file"""
        self.comments.append(comment)

    @property
    def taxa(self):
        if self._taxa is None:
            self._taxa = set()
            self._taxa.update(self.clusters_dict.keys())
        return self._taxa

    @property
    def clusters(self):
        if self._clusters is None:
            self._clusters = set()
            self._clusters.update(self.clusters_dict.values())
        return self._clusters

    def _make_taxa_block(self, autocolor=True):
        """Generates a Taxa block"""
        # '[&!color=#ff0033]'
        color_pallet = self.color_pallet()
        out = []
        for taxa, cluster in sorted(self.clusters_dict.items(), key=operator.itemgetter(0)):
            # out.append("\t%s--%s%s" % (self.clean(str(taxa)), cluster, ('[&!color= %s]' % color_pallet[int(
            out.append("\t%s%s" % (self.clean(str(taxa)), ('[&!color=%s]' % color_pallet[int(
                cluster)] if autocolor else '')))
        out[-1] = out[-1].strip(',')  # remove trailing comma
        out.append("\t;")
        return "\n".join(out)

    @staticmethod
    def generate_color_pallet(self, h_neg=0, h_pos=359, s=100, l=70, n=6):
        """hue (matiz ou tonalidade): Verifica o tipo de cor, abrangendo todas as cores do espectro,
                                      desde o vermelho até o violeta, mais o magenta. Atinge valores de 0 a 360,
                                      mas para algumas aplicações, esse valor é normalizado de 0 a 100%.
           saturation (saturação): Também chamado de "pureza". Quanto menor esse valor, mais com tom de cinza
                                   aparecerá a imagem. Quanto maior o valor, mais "pura" é a imagem. Atinge valores de
                                   0 a 100%.
           value (valor ou brilho): Define o brilho da cor. Atinge valores de 0 a 100%.

           Parameters:
                h_neg, h_pos : float in [0, 359]
                    Anchor hues for negative and positive extents of the map.
                s : float in [0, 100], optional
                    Anchor saturation for both extents of the map.
                l : float in [0, 100], optional
                    Anchor lightness for both extents of the map.
                n : int, optional
                    Number of colors in the palette (if not returning a cmap)
        """
        import colorsys

        # h_neg, h_pos, s=75, l=50, sep=10, n=6, center='light'
        # (150, 275, s=80, l=55, n=9)
        # (250, 15, s=75, l=40,  n=9, center="dark")
        # center : {“light”, “dark”}, optional
        #            Whether the center of the palette is light or dark


        # Normaliza os valores para ficar entre 0 e 1.
        h_neg /= 360.
        h_pos /= 360.
        s /= 100.
        l /= 100.

        delta_h_total = float(h_pos - h_neg)
        # corrige os ângulos para a passagem por 360 / 0º
        delta_h_total = (
            delta_h_total if delta_h_total > 0 else delta_h_total + 1)  # se negativo h_neg > h_pos, soma 360º

        delta_h = delta_h_total / float(n)

        # Calcula a paleta de cores
        HSV_pallet = []
        for x in xrange(n):
            h_color = (h_neg + delta_h / 2 + (delta_h * x))
            if h_color > 1:
                h_color -= 1
            HSV_pallet.append((h_color, s, l))

        # HSV_pallet = [(x * 1.0 / n, s, l) for x in xrange(n)]
        hex_out = []
        for rgb in HSV_pallet:
            rgb = map(lambda x: int(x * 255), colorsys.hsv_to_rgb(*rgb))
            hex_out.append("#" + "".join(map(lambda x: chr(x).encode('hex'), rgb)))
        return hex_out

    def color_pallet(self):
        """Generates colorize clusters"""
        if len(self.clusters) > 19:
           # pallet = self.generate_color_pallet(self, n=len(self.clusters))
            pallet = self.generate_color_pallet(self, n=max(len(self.clusters),max(sorted(map(int,self.clusters)))+1)) # damicore as vezes pula um ou outro n�mero de cluster por ser um cluster interno
        else:
            # from ._color_data import
            my_color_pallet = {'vermelho': '#ff0000',
                               'verde': '#00ff00',
                               'azul': '#0000ff',
                               'cinza': '#828282',
                               'rosa': '#ff00ff',
                               'ciano': '#00ffff',
                               'laranja': '#ff8000',
                               'pink': '#ff0080',
                               'roxo': '#8000ff',
                               'verde_limao': '#80ff00',
                               'verde_claro': '#00ff80',
                               'azul_claro': '#0080ff',
                               'preto': '#000000',
                               'azul_petroleo': '#3A6F85',
                               'verde_musgo': '#6F853A',
                               'roxo_suave': '#6F3A85',
                               'marrom': '#5A483B',
                               'vermlho_telha': '#AA483B',
                               'marrom_cafe': '#5A281B'}
            pallet = my_color_pallet.values()

        return pallet

    def make_nexus(self):
        """
        Generates a string representation of the nexus

        :return: String
        """
        assert len(self.tree) > 0, "No newick data!"
        assert len(self.taxa) > 0, "No taxa in cluster!"
        assert len(self.clusters_dict) > 0, "No clusters data!"

        return TEMPLATE.strip() % {
            'ntax': len(self.taxa),
            'taxlabels': self._make_taxa_block(),
            'newick_str': self.tree,
            'comments': self._make_comments(),
        }

    def write_to_file(self, filename="output.nex"):
        """
        Generates a string representation of the nexus

        :param filename: Filename to store nexus as
        :type filename: String

        :return: None
        """
        nexus = self.make_nexus()
        with open(filename, 'w+') as handle:
            handle.write(nexus)


if __name__ == '__main__':
    # NexusColorClustersWriter.py texts/001-tree.newick texts/001-membership.csv texts/001-tree.txt
    print 'Params=', sys.argv[1:]
    if len(sys.argv[1:]) <= 1:
        print("Params numbers must be: tree_filename, clusters_filename, and optionally out_filename")
    if len(sys.argv[1:]) == 2:
        tree_filename = sys.argv[1]
        clusters_filename = sys.argv[2]
        NexusColorClustersWriter(tree_filename, clusters_filename)
    if len(sys.argv[1:]) == 3:
        tree_filename = sys.argv[1]
        clusters_filename = sys.argv[2]
        out_filename = sys.argv[3]
        NexusColorClustersWriter(tree_filename, clusters_filename, out_filename)
