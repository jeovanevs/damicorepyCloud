#!/usr/bin/python
# coding=utf-8
# automatico para tipos de faltas

import os
import shutil
import sys
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from bson.objectid import ObjectId

import subprocess32
import time
import random
import glob

import logging
import csv


# class StreamToLogger(object):
#    """
#    Fake file-like stream object that redirects writes to a logger instance.
#    """
#    def __init__(self, logger, log_level=logging.INFO):
#       self.logger = logger
#       self.log_level = log_level
#       self.linebuf = ''
#
#    def write(self, buf):
#       for line in buf.rstrip().splitlines():
#          self.logger.log(self.log_level, line.rstrip())


def move_collection_data(src, dest, doc=None):
    for data in src.find(doc):
        dest.insert_one(data)
        if dest.find_one(data):
            result = src.delete_one(data)
            if result.deleted_count == 0:
                raise ValueError('Error deleting _id:%s from %s' % (data, src.full_name))
        else:
            raise ValueError('Error inserting _id:%s in %s' % (data, dest.full_name))


# logging
# logging.basicConfig(filename='/home/lsee/results.log', filemode='a', level=logging.DEBUG)
# stdout_logger = logging.getLogger('STDOUT')
# sys.stdout = LoggerWriter(stdout_logger, logging.debug)
# stderr_logger = logging.getLogger('STDERR')
# sys.stderr = LoggerWriter(stderr_logger, logging.warning)
# logging.basicConfig(
#    level=logging.DEBUG,
#    # format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
#    format='%(asctime)s:%(name)s:\t%(message)s',
#    filename="/home/lsee/results.log",
#    filemode='a'
# )
#
# stdout_logger = logging.getLogger('STDOUT')
# sl = StreamToLogger(stdout_logger, logging.CRITICAL)
# sys.stdout = sl
#
# stderr_logger = logging.getLogger('STDERR')
# sl = StreamToLogger(stderr_logger, logging.ERROR)
# sys.stderr = sl
# print 'teste'
# raise ValueError('teste')

# Iniciar comunicação com mongoDB
fault_types = ['A','AB','ABC','ABCT','ABT','AC','ACT','B','BC','BCT','C']
#fault_types = ['A']
#fault_types = ['B']
for fault_type in fault_types:
    print('Fault: ' + fault_type)
    connection_str = 'mongodb://10.235.2.227:27017/Feeder02'
    # connection_str = 'mongodb://192.168.80.143:27017/Feeder01'

    # create a connection
    client = MongoClient(connection_str)

    # check if the server is available
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        sys.stderr.write("\nServer not available")

    # select database
    db = client.get_database()

    # list collections on database
    collections = db.collection_names(include_system_collections=False)

    historical_collection_prefix = 'Faults_%s_' % fault_type
    regions = sorted([col.split(historical_collection_prefix)[1] for col in collections if historical_collection_prefix in col])
    regions = [elm for elm in regions if len(elm) < 5] #remove elementos como REG1_temp, etc.

#    regions = ['REG4']
    for REG in regions:# mudei para rodar uma vez só
        historical_collection = historical_collection_prefix+REG
        income_data_collection = 'incomeFaults_%s_%s' %(fault_type, REG)
        results_collection = 'results_%s_%s' % (fault_type, REG)
        temp_collection = 'Faults_%s_%s_temp' % (fault_type, REG)
        results_dir = '/home/team/damicorepy-parcial/damicore/dados/resultados/jeovane/%s/%s' % (fault_type, REG)
        # results_dir = '/home/lsee/Desktop/Dropbox/results'

        ### check if historical_collection exists
        # if historical_collection+REG not in collections:
        #     raise ValueError('Collection %s not in %s' % (historical_collection, collections))
        ## sempre vai existir

        # check if income_data collection exists
        if income_data_collection not in collections:
            # raise ValueError('Collection %s not in %s' % (income_data_collection, collections))
            # Cria income collection
            coll = db.create_collection(income_data_collection)
            db.command("collMod", coll.name,
                       validator={
                           "$and": [
                               {'EventTime': {"$type": "date"}},
                               {'FaultLocation': {"$type": "string"}},
                               {'FaultData': {"$elemMatch": {
                                   'MeasurementPoint': {"$type": "string"},
                                   'DCU_Id': {"$type": "binData"},
                                   'SM_Ids': {"$type": "binData"},
                                   'V3PHIrms': {"$elemMatch": {'$exists': True}},
                                   'ThetaV3PHI': {"$elemMatch": {'$exists': True}}, "$and": [
                                       {"$or": [{'I3PHIrms': {'$elemMatch': {'$exists': True}}}, {'I3PHIrms': []}]},
                                       {"$or": [{'ThetaI3PHI': {'$elemMatch': {'$exists': True}}},
                                                {'ThetaI3PHI': []}]}]
                               }}}]
                       },
                       validationLevel="strict",
                       validationAction="error")
            coll.create_index([('EventTime', pymongo.ASCENDING)], unique=True)
            # return index information
            # print(sorted(list(coll.index_information())))

        # check if result_data collection exists
        if results_collection not in collections:
            # raise ValueError('Collection %s not in %s' % (results_collection, collections))
            # cria results_collection
            db.create_collection(results_collection)

        income_data_collection = db[income_data_collection]
        temp_collection = db[temp_collection]
        historical_collection = db[historical_collection]
        results_collection = db[results_collection]

        # Preparando dados para minerar e ajustando condições iniciais
        sys.stderr.write('moving initial files')
        # Criar uma coleção temporaria e mover os dados pra lá
        move_collection_data(historical_collection, temp_collection)
        move_collection_data(income_data_collection, temp_collection)

        preload_data = 50 # começar a minerar com pelo menos 50 amostras
        docs_number = (temp_collection.count()-preload_data)/19 # N execuções além da primeira
        initial_docs = preload_data + (temp_collection.count()-preload_data) % docs_number

        # initial_docs = 4
        random_docs = temp_collection.aggregate([{"$sample": {"size": initial_docs}}])
        [move_collection_data(temp_collection, income_data_collection, doc) for doc in
         random_docs]  # only useful for partial NCD
        # [move_collection_data(temp_collection, historical_collection,doc) for doc in random_docs]
        # move_collection_data(temp_collection, historical_collection, temp_collection.find_one())
        # move_collection_data(temp_collection, historical_collection, temp_collection.find_one())

        sys.stderr.write('\nremoving previous files')

        # apagar os dados do banco de dados results
        result = results_collection.delete_many({})
        # apagar a pasta results
        shutil.rmtree(os.path.join(results_dir, income_data_collection.name), True)
        # shutil.rmtree(os.path.join(results_dir, historical_collection.name), True)

        count = income_data_collection.find().count()
        # count = historical_collection.find().count()
        sys.stderr.write('\nInitial Mining: %s\n' % count)
        sys.stderr.write('\nFault Type: %s %s\n' % (fault_type, REG))

        # Create the times file
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
        with open(os.path.join(results_dir, 'times.csv'), 'wt') as f:
            csv_writer = csv.DictWriter(f, fieldnames=["DB_data_fetch", "Mining", "DB_auto_move", "DB_results_insert",
                                                       "Total_time"])
            csv_writer.writeheader()

        # executar o damicore para gerar o result inicial
        t = time.time()

        # output = subprocess32.check_output(['./scratchMongo.py',
        subprocess32.call(['./scratchMongo.py',
                           connection_str,
                           income_data_collection.name,
                           historical_collection.name,
                           results_collection.name,
                           '--normalize-weights',
                           # '--normalize-matrix',
                           '--results-dir', results_dir,
                           '--auto-move-files',
                           '--o', '/dev/null'],
                          # stderr=subprocess32.STDOUT,)

                          # stderr=log,
                          )

        elapsed = time.time() - t  # tempo de processamento do damicore
        # print('\n' + output)
        sys.stderr.write('\nTotal Elapsed time (seconds):\t%s' % elapsed)

        # save times
        read = open(os.path.join(results_dir, 'times.csv'), 'rt')
        csv_reader = csv.DictReader(read)
        lines = [l for l in csv_reader]
        lines[-1].update({"Total_time": elapsed})
        read.close()
        write = open(os.path.join(results_dir, 'times.csv'), 'wt')
        csv_writer = csv.DictWriter(write, fieldnames=["DB_data_fetch", "Mining", "DB_auto_move", "DB_results_insert",
                                                       "Total_time"])
        csv_writer.writeheader()
        csv_writer.writerows(lines)
        write.close()

        if results_collection.count() != 1:
            raise ValueError('Error saving initial NCD matrix')

        # sys.stderr.write("\nUsing DAMICORE based on previous NCD matrix and one new random file")
        count1 = temp_collection.count()
        data_ids = list(temp_collection.find({}, {'_id': 1}))
        random.shuffle(data_ids)
        # count2 = 0

        for count2 in range(0, count1, docs_number):
            # for _id in data_ids:
            # os.system('clear')
            # count2+=1
            # sys.stderr.write('\n#############\nProcessing files %d-%d of %d, id: %s\n' % (count2+1,count2+docs_number,count,data_ids[count2:count2+docs_number]))
            sys.stderr.write(
                '\n#############\nProcessing files 1-%d of %d\n' % (count2 + docs_number + count, count1 + count))
            # mover dados da pasta de dados para o income (útil para o damicore partial)
            [move_collection_data(temp_collection, income_data_collection, _id) for _id in
             data_ids[count2:count2 + docs_number]]
            ## mover dados da pasta de dados para o historical
            # [move_collection_data(temp_collection, historical_collection,_id) for _id in data_ids[count2:count2+docs_number]]
            # move_collection_data(temp_collection,income_data_collection,_id)
            # executar o damicore parcial até acabar os dados
            t = time.time()
            subprocess32.call(['./scratchMongo.py',
                               connection_str,
                               income_data_collection.name,
                               historical_collection.name,
                               results_collection.name,
                               'last',
                               '--partial-ncd-mode',
                               '--normalize-weights',
                               # '--normalize-matrix',
                               '--results-dir', results_dir,
                               '--auto-move-files',
                               '--o', '/dev/null'
                               ])
            elapsed = time.time() - t
            sys.stderr.write('\nTotal Elapsed time (seconds):\t%s' % elapsed)

            # save times
            read = open(os.path.join(results_dir, 'times.csv'), 'rt')
            csv_reader = csv.DictReader(read)
            lines = [l for l in csv_reader]
            lines[-1].update({"Total_time": elapsed})
            read.close()
            write = open(os.path.join(results_dir, 'times.csv'), 'wt')
            csv_writer = csv.DictWriter(write,
                                        fieldnames=["DB_data_fetch", "Mining", "DB_auto_move", "DB_results_insert",
                                                    "Total_time"])
            csv_writer.writeheader()
            csv_writer.writerows(lines)
            write.close()

        sys.stderr.write('\nMining done!')
        income_data_collection.drop()
        temp_collection.drop()

        # sys.stderr.write('\nGenerating tree png')
        # for k in glob.glob(os.path.join(results_dir, income_data_collection.name, '*.nex')):
        #     sys.stderr.write('\n' + str(k))
        #     # os.system('java -jar ~/FigTree_v1.4.3/lib/figtree.jar -graphic PNG %s %s.png > /dev/null' % (k,k))
        #     os.system(
        #         'java -jar /home/lsee/Desktop/dependencies/FigTree_v1.4.3/lib/figtree.jar -graphic PNG %s %s.png > /dev/null' % (
        #             k, k))

sys.stderr.write('\nProcess finished')
