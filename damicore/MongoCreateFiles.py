import datasource
import os
import sys
import re
import argparse
import csv
import tempfile
import shutil

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure



####################################
def ncd_individual_compressed_sizes(ncd_results_list):
    """Converts a list of NcdResults to a dict {id:value} of compressed values.

     @param ncd_results_list List of NcdResult as returned by distance_matrix
     @return {ids:values} compression values of corresponding IDs
     """
    xs = [(result['x'], result['zx']) for result in ncd_results_list]
    ys = [(result['y'], result['zy']) for result in ncd_results_list]
    comp_sizes = sorted(set(xs + ys))

    return dict(comp_sizes)

def transpose(m):
    return [[m[j][i] for j in range(len(m))] for i in range(len(m[0]))]


def mongo_fault_data_factory_output_path(collection, output_dir=tempfile.gettempdir()):
    # Load the fault data
    faultData = list(collection.aggregate(
        [{"$project": {
            "FaultLocation": 1,
            "dataMatrix": {
                "$concatArrays": ["$FaultData.V3PHIrms",
                                  "$FaultData.ThetaV3PHI",
                                  {"$filter": {
                                      "input": "$FaultData.I3PHIrms",
                                      "as": "item",
                                      "cond": {"$ne": ["$$item", []]}}},
                                  {"$filter": {
                                      "input": "$FaultData.ThetaI3PHI",
                                      "as": "item",
                                      "cond": {"$ne": ["$$item", []]}}}
                                  ]
            }
        }
        }]))
    # transpose dataMatrix such as rows are the fases data and generate the sources
    sources = list()
    data_FaultLocations = {}
    for data in faultData:
        matrix = transpose(data['dataMatrix'])
        data.update(dataMatrix=matrix)
        # dataMatrix_str = '\r\n'.join([','.join(map('{:.9f}'.format, row)) for row in matrix])
        dataMatrix_str = '\r\n'.join([','.join(map('{:.7e}'.format, row)) for row in matrix])
        dataMatrix_str += "\r\n"
        sources.append(datasource.String(dataMatrix_str, tmp_dir=output_dir, name=str(data['_id'])))

        # FaultLocations tags (names)
        data_FaultLocations.update({str(data['_id']): data['FaultLocation']})

    # create the source factory from the collection
    return datasource.InMemoryFactory(sources), faultData, data_FaultLocations

#################################  main
def main(connection_str, output_path):
    try:

        if 'mongodb://' not in connection_str:
            raise ValueError('Connection string error')

        if len(connection_str.split('/')[-1].split('.')) != 2:
            raise ValueError('Collection not specified in the connection string. Please use database.collection to specify')

        collection_name = connection_str.split('.')[-1]
        uri = connection_str.strip('.' + collection_name)


        # create a connection
        client = MongoClient(uri)

        # check if the server is available
        try:
            # The ismaster command is cheap and does not require auth.
            client.admin.command('ismaster')
        except ConnectionFailure:
            print("Server not available")

        # select database from connection string
        db = client.get_database()

        # list collections on database
        collections = db.collection_names(include_system_collections=False)

        # check if the collection exist
        if collection_name not in collections:
            raise ValueError('Collection %s not in %s' % (collection_name, collections))

        # select collection
        data_collection = db[collection_name]

        # # print all documents in a collection
        # for doc in data_collection.find():
        #     pprint.pprint(doc)

        # create the new source factory form income collection
        data_factory, income_data, data_FaultLocations = mongo_fault_data_factory_output_path(data_collection, output_path);


        ###################################################
        sources = data_factory.get_sources()

        # create the source files and rename them to the _Id
        for datasrc in sources:
            source_name = datasrc.get_filename()
            path, name = os.path.split(source_name)
            rename = os.path.join(path,datasrc.name)
            shutil.move(source_name,rename)
            datasrc.filename = rename
            # #delete the file
            # datasrc.close()
        return {
            'factories': data_factory,
            'data': income_data,
            'fault_locations': data_FaultLocations,
            # 'previous_results': previous_results,
            'mongo_db': db,
            'mongo_collection': data_collection
        }
    finally:
        # close connection
        client.close()

##### Script entry point
if __name__ == '__main__':
    host = '192.168.80.143:27017'
    # if len(args.input) < 4:
    #     raise ValueError(
    #         'Invalid number of arguments: Provide at least the Database connection string, the income data collection'
    #         ', the historical storage collection, and the results output collection as inputs, respectively ')


    FaultType = 'AG'
    database = 'Feeder01'
    data_collection = 'Faults' + FaultType
    # Connection string
    connection_str = 'mongodb://%s/%s.%s' % (host, database, data_collection)

    output_path = '/home/lsee/Desktop/data2'

    out = main(connection_str, output_path)

    # print(out)