#!/usr/bin/python
#  coding=utf-8

import os
import sys
import re
import argparse
import csv
import tempfile

from pprint import pformat

import igraph

import _utils
import ncd2 as ncd
import ncd_base as ncdb
import partition as p
import clustering as c
import compressor as comp
import tree_simplification as nj
# from damicore import datasource
from tree import to_graph
# from datasource import create_factory
import datasource

from tree import newick_format
from _utils import normalize_list, dir_basename, open_outfile, get_version

# import damicore
from NexusColorClustersWriter import NexusColorClustersWriter
import shutil

import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from bson.objectid import ObjectId

import tree
import copy
import time

# Global times dict to store elapsed times
times = dict()


########################################3
# TODO(brunokim): use a cladogram layout, which igraph seems to be lacking
def graph_style(g, ids):
    style = {}
    seed_layout = g.layout('rt_circular')

    layout = g.layout('fr', seed=seed_layout.coords, weights='weight')
    style['layout'] = layout

    is_leafs = [(v['name'] in ids) for v in g.vs]
    style['vertex_size'] = [15 if is_leaf else 5 for is_leaf in is_leafs]
    style['vertex_label'] = [v['name'] if is_leaf else ''
                             for is_leaf, v in zip(is_leafs, g.vs)]

    return style


####################################
def ncd_individual_compressed_sizes(ncd_results_list):
    """Converts a list of NcdResults to a dict {id:value} of compressed values.

     @param ncd_results_list List of NcdResult as returned by distance_matrix
     @return {ids:values} compression values of corresponding IDs
     """
    xs = [(result['x'], result['zx']) for result in ncd_results_list]
    ys = [(result['y'], result['zy']) for result in ncd_results_list]
    comp_sizes = sorted(set(xs + ys))

    return dict(comp_sizes)


def transpose(m):
    return [[m[j][i] for j in range(len(m))] for i in range(len(m[0]))]


####################################
def parse_args(args):
    general_args = _utils.parse_args(args)
    verbosity = general_args['verbosity']
    # TODO: retornar os FaultLocations para o programa principal; Fechar a conexão com o banco de dados
    if len(args.input) < 4:
        raise ValueError(
            'Invalid number of arguments: Provide at least the Database connection string, the income data collection'
            ', the historical storage collection, and the results output collection as inputs, respectively ')

    connection_str = args.input[0]
    income_data_collection = args.input[1]
    historical_collection = args.input[2]
    results_collection = args.input[3]

    if 'mongodb://' not in connection_str:
        raise ValueError('Connection string error')

    # create a connection
    client = MongoClient(connection_str)

    # check if the server is available
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        print("Server not available")

    # select database from connection string
    db = client.get_database()

    # list collections on database
    collections = db.collection_names(include_system_collections=False)

    # check if historical_collection exists
    if historical_collection not in collections:
        raise ValueError('Collection %s not in %s' % (historical_collection, collections))

    # check if income_data collection exists
    if income_data_collection not in collections:
        raise ValueError('Collection %s not in %s' % (income_data_collection, collections))

    # check if result_data collection exists
    if results_collection not in collections:
        raise ValueError('Collection %s not in %s' % (results_collection, collections))

    # create the new source factory form income collection
    income_data_collection = db[income_data_collection]

    t = time.time();
    income_data_factory, income_data = mongo_fault_data_factory(income_data_collection);
    elapsed = time.time() - t  # tempo de busca ao banco de dados
    sys.stderr.write('\nincome data fetch time(seconds):\t%s\n\n' % elapsed)

    # store time
    times.update({"DB_data_fetch": elapsed})

    # FaultLocations tags (names)
    income_data_FaultLocations = {str(data['_id']): data['FaultLocation'] for data in income_data}

    # # create the previous source factory form history collection
    historical_collection = db[historical_collection]

    # generate the historical factory
    historical_factory, historical_data = mongo_fault_data_factory(historical_collection)
    # FaultLocations tags
    historical_data_FaultLocations = {str(data['_id']): data['FaultLocation'] for data in historical_data}

    # load the previous NCD results from input
    if args.partial_ncd_mode:

        if len(args.input) != 5:
            raise ValueError('The document with the NCD results to be used in partial-ndc-mode should be provided')

        previous_results_document = args.input[4]

        if previous_results_document == "last":
            previous_results = list(db[results_collection].find().sort('_id', pymongo.DESCENDING).limit(1))
            if not previous_results:
                raise ValueError('Empty previous results. You can run again without --partial-ndc-mode')
            previous_results = previous_results[0]
        else:
            previous_results = db[results_collection].find_one({"_id": ObjectId(previous_results_document)})
            if not previous_results:
                raise ValueError('Document with _id %s not in %s' % (previous_results_document, results_collection))

        previous_results_Faults = previous_results['Faults']
        previous_results = previous_results['ncdResults']
        # convert ObjectId to str
        previous_results = [
            {'zxy': res['zxy'], 'zx': res['zx'], 'zy': res['zy'], 'y': str(res['y']), 'x': str(res['x']),
             'ncd': res['ncd']} for res
            in previous_results]
        # previous_ncd_results = ncd.FilenameNcdResults('/home/lsee/Dropbox/DADOSResults/Dados/001-ncd.csv')
        # previous_results = list(previous_ncd_results.get_results())

        # read the individually compressed sizes from the previous NCD results
        individual_compressed_sizes = ncd_individual_compressed_sizes(previous_results)

        # check if some source name in the income datasource exists in the previous NCD file and ask what to do
        files_to_replace = []
        replace = 'N'
        auto_move_files = args.auto_move_files
        # replace the same name files or not?
        same_name_sources = [k for k in income_data_factory.name if individual_compressed_sizes.has_key(k)]
        if len(same_name_sources) > 0:
            if verbosity >= 1:
                if len(income_data_factory.name) == len(same_name_sources):
                    sys.stderr.write(
                        '##### All files in the present datasource are in the previous ncd matrix. ##### %s '
                        '\r\n ' % same_name_sources)
                else:
                    sys.stderr.write(
                        '\nThe following sources have the same name in the previous ncd matrix: \n%s \n' % same_name_sources)
                replace = raw_input('\r\nReplace previous NCD results? [Y/N] or Ctrl+c to cancel: ').capitalize()
                while replace not in ['Y', 'N']:
                    sys.stderr.write('Invalid answer!')
                    replace = raw_input('Replace previous results? [Y/N] or Ctrl+c to cancel: ').capitalize()
            else:
                replace = 'Y'

            if replace == 'Y':
                # TODO: verbosity considerations
                if verbosity >= 1:
                    sys.stderr.write('Replacing previous NCD results')
                # If some file in the history datasource have the same name in the income datasource remove the file from the history data source
                files_to_replace = [name for name in income_data_factory.name if name in historical_factory.name]
                for name in files_to_replace:
                    historical_factory.name.remove(name)
                    del historical_data_FaultLocations[name]

            else:  # no replace
                # Todo
                # If all files are the same and no replacement to do raise an error.
                if len(income_data_factory.name) == len(same_name_sources):
                    raise ValueError('Partial NCD mode (No replace): Error! The income data already in the NCD results')
                if verbosity >= 1:
                    sys.stderr.write('Keeping previous NCD results')
                files_to_replace = []
                auto_move_files = False
                # remove from the income sources the data already in the previous NCD results
                for name in same_name_sources:
                    for source in income_data_factory.sources:
                        if source.name == name:
                            income_data_factory.name.remove(source.name)
                            income_data_factory.sources.remove(source)
                            del income_data_FaultLocations[name]

        # check if the NCD matrix files are in the historical data source
        exclude_files_from_datasource = files_to_replace[:]  # Shallow copy
        checked_files = []
        for name in historical_factory.name:
            # check if data in the historical are in the previous NCD data
            if name in individual_compressed_sizes.keys():
                checked_files.append(name)
            else:  # if not exclude from data sources to be processed because they were not previously considered
                exclude_files_from_datasource.append(name)

        # Raise an error if some file is missing
        if len(checked_files) != len(individual_compressed_sizes) - len(files_to_replace):
            raise ValueError('The following documents were not founded : %s' %
                             [doc for doc in individual_compressed_sizes.keys() if
                              doc not in checked_files + files_to_replace])

        # If there are data documents that are not in the NCD matrix, or replaced, remove it from datasource factory
        if len(exclude_files_from_datasource) > 0:
            for name in exclude_files_from_datasource:
                for source in historical_factory.sources:
                    if source.name == name:
                        historical_factory.name.remove(source.name)
                        historical_factory.sources.remove(source)
                        del historical_data_FaultLocations[source.name]

        # retornar o par comprimido de acordo com os arquivos restantes
        if replace == 'Y':
            sources1 = sources2 = [source.name for source in historical_factory.get_sources()]
            pair_compressed_sizes = [result for result in previous_results for src1, src2 in
                                     ncd.pairs(sources1, sources2, is_upper=True) if
                                     (result['x'] == src1 and result['y'] == src2) or (
                                         result['y'] == src1 and result['x'] == src2)]

            individual_compressed_sizes = {k: individual_compressed_sizes[k] for k in sources1}
        else:
            pair_compressed_sizes = previous_results
        #TODO: sort datafactory
        # sorted(income_data_factory.name)

        income_data_factory.sources.sort(key=lambda src: src.name)
        income_data_factory.name = tuple(src.name for src in income_data_factory.sources)
        historical_factory.sources.sort(key=lambda src: src.name)
        historical_factory.name = tuple(src.name for src in historical_factory.sources)
        factories = [income_data_factory, historical_factory]

    else:
        # check if some source name in the income datasource exists in the history datasource
        same_name_sources = [k for k in income_data_factory.name if k in historical_factory.name]
        if len(same_name_sources) > 0:
            if len(income_data_factory.name) == len(same_name_sources):
                raise ValueError('##### All files in the income datasource are in the historical datastore. ##### %s '
                                 '\r\n ' % same_name_sources)
            else:
                raise ValueError(
                    '\nThe following sources in the income datafactory have the same _id in the historical datastore: \n%s \n' % same_name_sources)

        previous_results = []
        individual_compressed_sizes = {}
        pair_compressed_sizes = []
        auto_move_files = args.auto_move_files
        # sort the datasources before process the NCD or it may be different every run
        factories = [datasource.InMemoryFactory(sorted(income_data_factory.sources + historical_factory.sources,key=lambda source: source.name))]

    ###################################################

    income_data_FaultLocations.update(historical_data_FaultLocations)
    compressor = comp.get_compressor(args.compressor, level=args.level,
                                     model_order=args.model_order, memory=args.memory,
                                     restoration_method=args.restoration)

    #    if args.auto_move_files and replace=='Y': auto_move_files = True
    #    else: auto_move_files = False

    # with open(os.path.join('/tmp/0_tags.log'),'a') as f:
    # f.write(str(income_data_FaultLocations))
    # print(income_data_FaultLocations)

    return {
        'factories': factories,
        'compressor': compressor,
        'matrix_format': args.matrix_format,
        'individual_compressed_sizes': individual_compressed_sizes,
        'pair_compressed_sizes': pair_compressed_sizes,
        'previous_results': previous_results,
        'auto_move_files': auto_move_files,
        'files_to_move': income_data_factory.name,
        'fault_locations': income_data_FaultLocations,
        'mongo_db': db,
        'mongo_collections': (income_data_collection, historical_collection, db[results_collection])
    }


def mongo_fault_data_factory(collection):
    # Load the fault data
    faultData = list(collection.aggregate(
        [{"$project": {
            "FaultLocation": 1,
            "dataMatrix": {
                "$concatArrays": ["$FaultData.V3PHIrms",
                                  "$FaultData.ThetaV3PHI",
                                  {"$filter": {
                                      "input": "$FaultData.I3PHIrms",
                                      "as": "item",
                                      "cond": {"$ne": ["$$item", []]}}},
                                  {"$filter": {
                                      "input": "$FaultData.ThetaI3PHI",
                                      "as": "item",
                                      "cond": {"$ne": ["$$item", []]}}}
                                  ]
            }
        }
        }]))
    # transpose dataMatrix such as rows are the fases data and generate the sources
    sources = list()
    for data in faultData:
        matrix = transpose(data['dataMatrix'])
        data.update(dataMatrix=matrix)
        # dataMatrix_str = '\r\n'.join([','.join(map('{:.9f}'.format, row)) for row in matrix])
        dataMatrix_str = '\r\n'.join([','.join(map('{:.7e}'.format, row)) for row in matrix])
        dataMatrix_str += "\r\n"
        sources.append(datasource.String(dataMatrix_str, tmp_dir=tempfile.gettempdir(), name=str(data['_id'])))

    # create the source factory from the collection
    return datasource.InMemoryFactory(sources), faultData


####################################
def partial_distance_matrix(factories, compressor, previous_results, is_parallel=True,
                            individual_compressed_sizes=None, pair_compressed_sizes=None,
                            verbosity=0, tmp_dir=tempfile.gettempdir(),
                            interleave_block_size=0):
    """Calculates matrix of distances between all sources provided by a factory.
    """
    if not factories or len(factories) != 2:
        raise ValueError, 'Invalid factories argument'

    # testar se factories estão de acordo
    if len(factories[0].name) < 1:
        sys.stderr.write('No new file to compress, using previous NCD data\n')
        return ncd.InMemoryNcdResults(previous_results)  # retornar os valores antigos
    else:
        factory1, factory2 = factories
        sources1, sources2 = factory1.get_sources(), factory2.get_sources()
        # sources = sources1 + sources2

    if verbosity >= 1:
        sys.stderr.write('Compressing individual sources...\n')
    compressed_sizes = ncd.compress_sources(sources1, compressor, is_parallel)
    compressed_sizes.update(individual_compressed_sizes)

    if verbosity >= 1:
        sys.stderr.write('Compressing source pairs...\n')

    # Create (nxn)-n matrix with new files
    partial_pairs = ncd.ncd_pairs(sources1, sources1, compressor, compressed_sizes,
                                  is_upper=True, is_parallel=is_parallel, tmp_dir=tmp_dir,
                                  interleave_block_size=interleave_block_size)

    partial_pairs = list(partial_pairs.get_results())

    # Create (mxn) matrix with new files and previous files
    if len(sources2) > 0:
        partial_pairs2 = ncd.ncd_pairs(sources1, sources2, compressor, compressed_sizes,
                                       is_upper=False, is_parallel=is_parallel, tmp_dir=tmp_dir,
                                       interleave_block_size=interleave_block_size)

        partial_pairs2 = list(partial_pairs2.get_results())
    else:
        partial_pairs2 = []

    # sort_results = lambda rs: sorted(list(rs), key=lambda r: (r['x'], r['y']))

    return ncd.InMemoryNcdResults(sorted(partial_pairs + partial_pairs2 + pair_compressed_sizes,
                                         key=lambda r: (r['x'], r['y'])))


####################################
# clustering.pipeline function considering previously computed NCD and new files
def pipeline2(factories=None, compressor=None, ncd_results=None,
              individual_compressed_sizes=None, pair_compressed_sizes=None,
              is_parallel=True,
              is_normalize_matrix=True, is_normalize_weights=True, num_clusters=None,
              community_detection_name='fast', verbosity=0,
              is_partial_ncd=False, previous_results=None):
    if not is_partial_ncd:
        return c.pipeline(factories, compressor, ncd_results,
                          is_parallel=is_parallel,
                          is_normalize_weights=is_normalize_weights,
                          is_normalize_matrix=is_normalize_matrix,
                          num_clusters=num_clusters,
                          community_detection_name=community_detection_name,
                          verbosity=verbosity)
    else:
        if verbosity >= 1:
            if is_partial_ncd:
                sys.stderr.write('Performing partial NCD distance matrix calculation with new files...\n')

        # ncd_results_list = partial_distance_matrix(factories, compressor,  # generator to ncd_results
        #                                            individual_compressed_sizes=individual_compressed_sizes,
        #                                            pair_compressed_sizes=pair_compressed_sizes,
        #                                            is_parallel=is_parallel, verbosity=verbosity)
        #
        #
        # return _pipeline(ncd_results_list, is_normalize_matrix, is_normalize_weights,
        #                  num_clusters, community_detection_name, verbosity)
        if not previous_results:
            raise ValueError('No previous result to perform partial NCD')
        ncd_results = partial_distance_matrix(factories, compressor,  # generator to ncd_results
                                              individual_compressed_sizes=individual_compressed_sizes,
                                              pair_compressed_sizes=pair_compressed_sizes,
                                              is_parallel=is_parallel, verbosity=verbosity,
                                              previous_results=previous_results)

        return _pipeline(ncd_results, is_normalize_matrix, is_normalize_weights,
                         num_clusters, community_detection_name, verbosity)


###############################################################################################
def _pipeline(ncd_results_list,
              is_normalize_matrix=True, is_normalize_weights=True, num_clusters=None,
              community_detection_name='fast', verbosity=0):
    ncd_results_list = list(ncd_results_list.get_results())  # previously generated results

    if verbosity >= 1:
        sys.stderr.write('Simplifying graph...\n')
    if not is_normalize_matrix:
        m, (ids, _) = ncd.to_matrix(ncd_results_list)
    else:
        ds = normalize_list(list(result['ncd'] for result in ncd_results_list))
        normalized_results = [ncd.NcdResult(result, ncd=dist)
                              for result, dist in zip(ncd_results_list, ds)]
        m, (ids, _) = ncd.to_matrix(normalized_results)

    tree = nj.neighbor_joining(m, ids)

    if verbosity >= 1:
        sys.stderr.write('Clustering elements...\n')
    g = to_graph(tree)
    membership, clustering, dendrogram = c.tree_clustering(g, ids,
                                                           is_normalize_weights=is_normalize_weights,
                                                           num_clusters=num_clusters,
                                                           community_detection_name=community_detection_name)

    return {
        'ncd_results': ncd_results_list,
        'phylo_tree': tree,
        'leaf_ids': ids,
        'tree_graph': g,
        'dendrogram': dendrogram,
        'membership': membership,
        'clustering': clustering,
    }


###############################################################################################
def main(a):
    general_args = _utils.parse_args(a)
    clustering_args = c.parse_args(a)
    output, is_parallel, verbosity = (general_args['output'],
                                      general_args['is_parallel'], general_args['verbosity'])
    is_normalize_weights, is_normalize_matrix, num_clusters = (
        clustering_args['is_normalize_weights'],
        clustering_args['is_normalize_matrix'], clustering_args['num_clusters'])
    community_detection_name, ncd_results = (
        clustering_args['community_detection_name'],
        clustering_args['ncd_results'])

    if a.matrix_mode and a.partial_ncd_mode:
        raise ValueError('matrix-mode does not work in partial-ncd matrix calculation')

    # parse input args
    # print(' '.join(sys.argv[1:]))
    partial_ncd_args = parse_args(a)

    factories, compressor, matrix_format = (partial_ncd_args['factories'],
                                            partial_ncd_args['compressor'],
                                            partial_ncd_args['matrix_format'])

    individual_compressed_sizes = partial_ncd_args['individual_compressed_sizes']
    pair_compressed_sizes = partial_ncd_args['pair_compressed_sizes']
    previous_results = partial_ncd_args['previous_results']
    auto_move_files = partial_ncd_args['auto_move_files']
    files_to_move = partial_ncd_args['files_to_move']
    fault_locations = partial_ncd_args['fault_locations']
    mongo_collections = partial_ncd_args['mongo_collections']
    mongo_db = partial_ncd_args['mongo_db']

    # import time

    t = time.time()

    result = pipeline2(factories, compressor, ncd_results,
                       individual_compressed_sizes, pair_compressed_sizes,
                       is_parallel=is_parallel,
                       is_normalize_weights=is_normalize_weights,
                       is_normalize_matrix=is_normalize_matrix,
                       num_clusters=num_clusters,
                       community_detection_name=community_detection_name,
                       verbosity=verbosity,
                       is_partial_ncd=a.partial_ncd_mode, previous_results=previous_results)

    elapsed = time.time() - t
    sys.stderr.write('\nMining data time(seconds):\t%s\n' % elapsed)
    times.update({"Mining": elapsed})

    ncd_results = result['ncd_results']
    phylo_tree = result['phylo_tree']
    tree_graph = result['tree_graph']
    leaf_ids = result['leaf_ids']
    membership = result['membership']
    clustering = result['clustering']
    dendrogram = result['dendrogram']

    if auto_move_files:
        if not mongo_collections:
            raise ValueError('No collection provided')
        if files_to_move:
            t = time.time()
            src_coll, dest_coll = mongo_collections[0:2]

            for src in files_to_move:
                src_doc = src_coll.find_one({'_id': ObjectId(src)})

                if not src_doc:
                    raise ValueError('_id:%s not found in %s' % (src, src_coll.full_name))
                # insert doc in dest_coll
                dest_coll.insert_one(src_doc)
                # check if doc was inserted in dest_coll and remove from src_coll
                if dest_coll.find_one({'_id': ObjectId(src)}):
                    delete_result = src_coll.delete_one({'_id': ObjectId(src)})
                    if delete_result.deleted_count == 0:
                        raise ValueError('Error deleting _id:%s from %s' % (src, src_coll.full_name))
                else:
                    raise ValueError('Error inserting _id:%s in %s' % (src, dest_coll.full_name))

            elapsed = time.time() - t
            sys.stderr.write('\nauto move data time(seconds):\t%s\n' % elapsed)
            times.update({"DB_auto_move": elapsed})

    # preservar valores anteriores para comparação
    # ncd_results = copy.deepcopy(ncd_results)
    # phylo_tree = copy.deepcopy(phylo_tree)
    # tree_graph = copy.deepcopy(tree_graph)
    # leaf_ids = copy.deepcopy(leaf_ids)
    # membership = copy.deepcopy(membership)
    # clustering = copy.deepcopy(clustering)
    # dendrogram = copy.deepcopy(dendrogram)

    [res.update({'y': ObjectId(res['y']), 'x': ObjectId(res['x'])}) for res in ncd_results]
    mongo_result = {
        'Faults': [{'id': ObjectId(k), 'location': fault_locations[k], 'cluster': membership[k]} for k in
                   fault_locations],
        'ncdResults': ncd_results,
        'phyloTree': newick_format(phylo_tree)
    }

    t = time.time();
    mongo_collections[2].insert_one(mongo_result);
    elapsed = time.time() - t
    sys.stderr.write('\nresult data insert time(seconds):\t%s\n\n' % elapsed)
    times.update({"DB_results_insert": elapsed})

    mongo_db.client.close()

    # substituir ids pelos Faultlocations nos arquivos finais
    replace_ids = True

    if replace_ids:
        def update_Node(Node, fault_locations):
            for edge in Node.edges:
                if isinstance(edge.dest, tree.Leaf):
                    # print('%s --> %s' % (edge.dest.content, fault_locations[edge.dest.content]))
                    edge.dest.content = str(fault_locations[edge.dest.content])
                else:
                    update_Node(edge.dest, fault_locations)

        update_Node(phylo_tree, fault_locations)
        phylo_tree

        [res.update({'y': fault_locations[str(res['y'])], 'x': fault_locations[str(res['x'])]}) for res in ncd_results]
        ncd_results

        weights = tree_graph.es['weight']
        tree_graph = to_graph(phylo_tree)
        tree_graph.es['weight'] = weights

        leaf_ids = [fault_locations[id] for id in leaf_ids]

        membership = {v: membership[k] for k, v in fault_locations.items()}
        clustering._graph = tree_graph
        dendrogram._graph = tree_graph

    # Outputs NCD step
    if a.ncd_output:
        with open(a.ncd_output, 'wt') as f:
            if matrix_format == 'phylip':
                ncd.write_phylip(f, ncd_results)
            else:
                ncdb.csv_write(f, ncd_results, output_header=True)

    # Outputs tree in Newick format
    if a.tree_output:
        with open(a.tree_output, 'wt') as f:
            f.write(newick_format(phylo_tree))

    # Outputs graph image
    if a.graph_image:
        tree_style = graph_style(tree_graph, leaf_ids)
        igraph.plot(clustering, target=a.graph_image, **tree_style)

    # Outputs clustering result
    with open_outfile(output) as f:
        f.write(p.membership_csv_format(membership))

    # Outputs clustered philotree results in a Nexus format
    if a.tree_output and output:
        nexus = NexusColorClustersWriter()

        with open(a.tree_output, 'rt') as f:
            nexus.read_tree_string(f.read())

        with open(output, 'rt') as f:
            nexus.read_clusters_string(f.read())

        nexus.write_to_file(os.path.splitext(a.tree_output)[0] + '.nex')

    # Outputs index, if comparison reference was provided
    if a.compare_to:
        with open_outfile(a.partition_output) as f:
            reference_cluster = p.membership_parse(a.compare_to, as_clusters=True)
            obtained_cluster = p.membership_to_clusters(membership)
            index = p.compare_clusters(reference_cluster, obtained_cluster,
                                       index_name=a.partition_index)
            f.write('%s\n' % pformat(index))

    # Output everything to directory
    if a.results_dir:
        # Create dir if it does not exist
        if not os.path.exists(a.results_dir):
            os.mkdir(a.results_dir)

        # Creates subdirectory containing results for this run
        subdirname = dir_basename(a.input[1])
        subpath = os.path.join(a.results_dir, subdirname)
        if not os.path.exists(subpath):
            os.mkdir(subpath)

        # Finds maximum index in subdirectory
        fnames = os.listdir(subpath)
        matches = [re.match('\d+', fname) for fname in fnames]
        indices = [int(match.group(0)) for match in matches if match]
        max_index = max([0] + indices)  # if indices is empty, return 0
        index = max_index + 1

        # Writes all times
        with open(os.path.join(a.results_dir, 'times.csv'), 'at') as f:
            csv_writer = csv.DictWriter(f, fieldnames=["DB_data_fetch", "Mining", "DB_auto_move", "DB_results_insert"])
            csv_writer.writerow(times)

        # Writes all results
        base = os.path.join(subpath, '%03d-' % index)
        with open(base + 'version', 'wt') as f:
            f.write(get_version())
        with open(base + 'args.txt', 'wt') as f:
            f.write('\n'.join(sys.argv[1:]))
        with open(base + 'ncd.csv', 'wt') as f:
            ncd.csv_write(f, ncd_results)
        with open(base + 'ncd.phylip', 'wt') as f:
            ncd.write_phylip(f, ncd_results)
        with open(base + 'tree.newick', 'wt') as f:
            f.write(newick_format(phylo_tree))
        with open(base + 'membership.csv', 'wt') as f:
            f.write(p.membership_csv_format(membership))
        if a.compare_to:
            with open(base + 'partition.csv', 'wt') as f:
                all_indices = p.compare_clusters(reference_cluster, obtained_cluster,
                                                 index_name='all')
                writer = csv.DictWriter(f, fieldnames=['index name', 'value'])
                writer.writeheader()
                writer.writerows({'index name': k, 'value': v}
                                 for k, v in all_indices.items())

        tree_style = graph_style(tree_graph, leaf_ids)
        igraph.plot(clustering, target=base + 'tree.svg', **tree_style)

        nexus = NexusColorClustersWriter()
        with open(base + 'tree.newick', 'rt') as f:
            nexus.read_tree_string(f.read())
        with open(base + 'membership.csv', 'rt') as f:
            nexus.read_clusters_string(f.read())
        nexus.write_to_file(base + 'clustertree.nex')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(parents=[_utils.cli_parser(), ncd.cli_parser(), c.cli_parser()],
                                     fromfile_prefix_chars='+',
                                     description="Compute the clustering of a given dataset using the DAMICORE" +
                                                 " methodology.")

    parser.add_argument('--ncd-output', help='File to output NCD result',
                        metavar="FILE")
    parser.add_argument('--tree-output', help='File to output tree result',
                        metavar="FILE")
    parser.add_argument('--graph-image', help='File to output graph image',
                        metavar="FILE")

    parser.add_argument('--results-dir', metavar="DIR",
                        help='Directory to output all obtained results')

    parser.add_argument('--partial-ncd-mode', action='store_true',
                        help='Compute compressions of the new files only')

    parser.add_argument('--auto-move-files', action='store_true',
                        help='Move new source files to the previous NCD source folder after'
                             'NCD calculation')

    partition_group = parser.add_argument_group('Clustering comparison options',
                                                'Options for comparison between partitions')
    partition_group.add_argument('--compare-to',
                                 help='Reference membership file to compare the resulting clustering',
                                 metavar="FILE")
    partition_group.add_argument('--partition-index', choices=p.list_indices(),
                                 default='wallace', help='Partition comparison index to use')
    partition_group.add_argument('--partition-output', metavar="FILE",
                                 help="File to output partition comparison (default:stdout)")

    a = parser.parse_args()

    main(a)
