#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 18:34:43 2017
# print all documents in a collection
@author: lsee
"""
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

import pprint  # pretty print

import datetime as dt
import time
import uuid
import os


def get_filenames(directory):
    fnames = sorted(os.listdir(directory))
    return [os.path.join(directory, fname)
            for fname in fnames
            if os.path.isfile(os.path.join(directory, fname))]


# função para selecionar uma coluna em uma matriz
# def nCol(mat,col)
mCol = lambda mat, col: [row[col] for row in mat]

fault_types = ['AG', 'BG', 'CG', 'ABG', 'ACG', 'BCG', 'ABCG', 'AB', 'AC', 'BC', 'ABC']


def main(connection_str, fault_type):
    if fault_type not in fault_types:
        raise ValueError('Fault type %s not in %s' % (fault_type, fault_types))

    if 'mongodb://' not in connection_str:
        raise ValueError('Connection string error')

    if len(connection_str.split('/')[-1].split('.')) != 2:
        raise ValueError('Collection not specified in the connection string. Please use database.collection to specify')

    collection_name = connection_str.split('.')[-1]
    uri = connection_str.strip('.' + collection_name)

    # create a connection
    client = MongoClient(uri)

    try:
        # check if the server is available
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        print("Server not available")

    # # list databases
    # dataBases = client.database_names()

    # select database
    db = client.get_database()

    # list collections on database
    collections = db.collection_names(include_system_collections=False)

    # select collection
    if collection_name not in collections:
        raise ValueError('Collection %s not in %s' % (collection_name, collections))
    else:
        coll = db['Faults' + fault_type]

    # print all documents in a collection
    for doc in coll.find():
        pprint.pprint(doc)

    # close connection
    client.close()


if __name__ != '__main__':
    raise ImportError('This script will not run as module')

################ Main ########################
if __name__ == '__main__':
    host = '192.168.80.143:27017'
    FaultType = 'AG'
    # Connection string
    connection_str = 'mongodb://%s/%s.%s' % (host, 'Feeder01', 'Faults' + FaultType)
    main(connection_str, FaultType)
